const http = require('http')
const server = require('./server/server')
const sequelize = require('./database/sequelize')
const port = process.env.PORT || 4000
const redis = require('./database/redis')
const upload_csv = require('./script')
const cron = require("node-cron");

const runServer = async () => {

    // Monitor redis events running fast
    redis.monitor(function (err, monitor) {
        // Entering monitoring mode.
        monitor.on('monitor', function (time, args, source, database) {
            // console.log(time + ": " + args);
        });
    });
    //comment
    try {
        // connecting and syncing to the database
        //await sequelize.sync({ force: true })
        //await sequelize.sync()
    //   await sequelize.sync({ alter: true })
        console.log('Connected to the database successfully and syncing tables ...')
    } catch (error) {
        console.error(error)
    }

    try {
        // creating express server
        const myserver = http.createServer(server)
        myserver.listen(port)
        console.log('Server running..... Port -', port)
    } catch (error) {
        console.error(error)
    }

    // cron for deleting unused images in the temp storage
    cron.schedule("* */1 * * *", async function () {
        await new s3().uploadedFileDeletedToS3()
    });

    try {
        upload_csv()
    } catch (error) {
        //console.log('Error in uploading csv', error)
    }
}

runServer()

