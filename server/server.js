const express = require('express')
const app = express()
const morgan = require('morgan')
const winston = require('winston')
const helmet = require("helmet");
const swaggerJsDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')
const { errors } = require('celebrate')
var cors = require('cors')

require('dotenv').config()


// Body Parser
app.use(express.json({ limit: '200mb' }));
app.use(express.urlencoded({ limit: '200mb', extended: true }));



// Securing Application API's
app.use(helmet())



app.use(morgan('tiny'))


const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  defaultMeta: { service: 'auth-service' },
  transports: [
    new winston.transports.File({ filename: 'error.log', level: 'error' }),
    new winston.transports.File({ filename: 'combined.log' }),
  ],
});
if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({
    format: winston.format.simple(),
  }));
}


app.use((req, res, next) => {
  console.log('some change')
  next()
})

// handle cors Errors
// app.options('*', cors())
app.use(cors());

app.get('/_health_app', (req, res) => {
  res.status(200).send(true)
})


//expose static files
app.use(express.static('static'));

// routes to be published here
app.get('/_health_content', (req, res) => {
  res.status(200).send(true)
})


// Error Handling
app.use((req, res, next) => {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});

app.use(errors())
// app.use(errorHandler(logger));



module.exports = app