const path = require('path')
const multer = require('multer')
const _ = require('lodash')
let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'static/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '.' + file.originalname.split('.').pop())
    }
})
let upload = multer({
    storage: storage,
    fileFilter: function (req, file, callback) {
        ////console.log(file)
        let ext = path.extname(file.originalname);
        let fileType = file.mimetype.split('/').shift()
        const extensions = ['.png', '.PNG', '.jpg', '.jpeg', '.gif', '.jfif', '.JPG', '.JPEG', '.json', '.JSON']
        if (!(_.isEqual(fileType, 'image') || _.isEqual(file.mimetype, 'application/json'))) {
            return callback(new Error('Only image file, gifs and json are allowed.'))
        }
        if (!_.includes(extensions, ext)) {
            return callback(new Error('Invalid file extension'))
        }
        callback(null, true)
    },
    limits: {
        fileSize: 1024 * 1024 * 5
    }
})
module.exports = upload