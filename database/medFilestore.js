const path = require('path')
const multer = require('multer')
const _ = require('lodash')
let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'static/')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '.' + file.originalname.split('.').pop())
    }
})
let upload = multer({
    storage: storage,
    fileFilter: function (req, file, callback) {
        ////console.log(file)
        callback(null, true)
    },
    limits: {
        fileSize: 1024 * 1024 * 5
    }
})
module.exports = upload