const _ = require('lodash')
const moment = require('moment')
module.exports = (sequelize, type) => {
    const key = sequelize.define('key', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        title: {
            type: type.TEXT,
            allowNull: false
        },
        sortNumber: {
            type: type.INTEGER,
            allowNull: true,
        },
        description: {
            type: type.JSONB,
            allowNull: false
        },
        active: {
            type: type.BOOLEAN,
            allowNull: true,
            defaultValue: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        type: {
            type: type.INTEGER,
            allowNull: false
        },
        totalMcqs: {
            type: type.INTEGER,
            allowNull: true
        },
        totalQnas: {
            type: type.INTEGER,
            allowNull: true
        },
        totalMccqs: {
            type: type.INTEGER,
            allowNull: true
        },
        totalAtf: {
            type: type.INTEGER,
            allowNull: true
        },
        audioUrl: {
            type: type.TEXT,
            allowNull: true
        },

    }, {
        underscored: true,
        freezeTableName: true,
        hooks: {
            afterCreate: (instance, options) => {
                ////console.log('instance-----------------', instance)
                sequelize.model('topic').findOne({ where: { id: instance.dataValues.topicId } }).then(val => {
                    ////console.log('topicVal', val)
                    let totalKeys = 1
                    if (val.dataValues.totalKeys) totalKeys = val.dataValues.totalKeys + totalKeys
                    sequelize.model('topic').update({ totalKeys }, { where: { id: instance.dataValues.topicId }, fields: ['totalKeys'] })
                })
                sequelize.model('subject').findOne({ where: { id: instance.dataValues.subjectId } }).then(val => {
                    let noOfKeys = 1
                    if (val && val.dataValues && val.dataValues.totalKeys) {
                        noOfKeys = val.dataValues.totalKeys + noOfKeys
                    }
                    sequelize.model('subject').update({ totalKeys: noOfKeys }, { where: { id: instance.dataValues.subjectId }, fields: ['totalKeys'] })
                })
            },
            beforeBulkUpdate: async (instance, options) => {
                ////console.log('Beforfe instance-----------------', instance)

                if (_.includes(instance.fields, 'topicId')) {
                    const keyData = await sequelize.model('key').findOne({ where: { uuidIdentifier: instance.where.uuidIdentifier } })

                    const topicId = keyData.dataValues.topicId

                    // update the key count
                    const topic = await sequelize.model('topic').findOne({ where: { id: topicId } })
                    let count = topic.dataValues.totalKeys
                    if (count && count > 0) {
                        count = count - 1
                        await sequelize.model('topic').update({ totalKeys: count }, { where: { id: topicId }, fields: ['totalKeys'] })
                    }
                }
                if (_.includes(instance.fields, 'subjectId')) {
                    const keyData = await sequelize.model('key').findOne({ where: { uuidIdentifier: instance.where.uuidIdentifier } })
                    const subjectId = keyData.dataValues.subjectId
                    const subject = await sequelize.model('subject').findOne({ where: { id: subjectId } })
                    let count1 = subject.dataValues.totalKeys
                    if (count1 && count1 > 0) {
                        count1 = count1 - 1
                        await sequelize.model('subject').update({ totalKeys: count1 }, { where: { id: subjectId }, fields: ['totalKeys'] })
                    }
                }
            },
            afterBulkUpdate: async (instance, options) => {
                ////console.log('after instance-----------------', instance)
                if (_.includes(instance.fields, 'topicId')) {

                    const topic = await sequelize.model('topic').findOne({ where: { id: instance.attributes.topicId } })
                    let count = topic.dataValues.totalKeys
                    ////console.log(count)
                    if (count) count = count + 1
                    else count = 1
                    await sequelize.model('topic').update({ totalKeys: count }, { where: { id: instance.attributes.topicId }, fields: ['totalKeys'] })
                    if (_.includes(instance.fields, 'subjectId')) {

                        // update the subject count
                        const subject = await sequelize.model('subject').findOne({ where: { id: instance.attributes.subjectId } })
                        let count1 = subject.dataValues.totalKeys

                        if (count1) count1 = count1 + 1
                        else count1 = 1
                        await sequelize.model('subject').update({ totalKeys: count1 }, { where: { id: instance.attributes.subjectId }, fields: ['totalKeys'] })

                    }
                }
            }
        }


    })
    // key relationship

    key.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    key.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    key.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })
    key.belongsTo(sequelize.model('subject'),
        {
            foreignKey: {
                allowNull: false
            }
        })

    key.belongsTo(sequelize.model('topic'),
        {
            foreignKey: {
                allowNull: false
            }
        })

    return key
}

/**
 * @swagger
 *  components:
 *    schemas:
 *      KeyWithoutId:
 *        type: object
 *        required:
 *          - title
 *          - sortNumber
 *          - subjectId
 *          - topicId
 *          - active
 *          - type
 *          - description
 *        properties:
 *          title:
 *            type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          description:
 *            type: array
 *            items:
 *              type: object
 *              anyOf:
 *                 - $ref: '#/components/schemas/KeyText'
 *                 - $ref: '#/components/schemas/KeyPlainText'
 *                 - $ref: '#/components/schemas/KeyImage'
 *                 - $ref: '#/components/schemas/KeyVideo'
 *                 - $ref: '#/components/schemas/KeySlider'
 *          subjectId:
 *            type: string
 *            format: uuid
 *          topicId:
 *            type: string
 *            format: uuid
 *          active:
 *            type: boolean
 *          type:
 *            type: string
 *            enum: [milestoneKey, tips&tricks, summary, normal]
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      KeyForUpdate:
 *        type: object
 *        properties:
 *          title:
 *            type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          description:
 *            type: array
 *            items:
 *              type: object
 *          subjectId:
 *            type: string
 *            format: uuid
 *          topicId:
 *            type: string
 *            format: uuid
 *          active:
 *            type: boolean
 *          type:
 *            type: string
 *            enum: [milestoneKey, tips&tricks, summary, normal]
 */





/**
 * @swagger
 *  components:
 *    schemas:
 *      KeyWithId:
 *        type: object
 *        properties:
 *          uuidIdentifier:
 *            type: uuid
 *          title:
 *            type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          description:
 *            type: array
 *            items:
 *              type: object
 *          subjectId:
 *            type: string
 *            format: uuid
 *          topicId:
 *            type: string
 *            format: uuid
 *          createdBy:
 *            type: string
 *            $ref: '#/components/schemas/CreatedBy'
 *          updatedBy:
 *            type: string
 *            $ref: '#/components/schemas/CreatedBy'
 *          deletedBy:
 *            type: string
 *            $ref: '#/components/schemas/CreatedBy'
 *          deletedAt:
 *            type: string
 *            format: date
 *          updatedAt:
 *            type: string
 *            format: date
 *          createdAt:
 *            type: string
 *            format: date
 *          active:
 *            type: boolean
 *          type:
 *            type: string
 *            enum: [milestoneKey, tips&tricks, summary, normal]
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      KeyForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: string
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              recover:
 *                type: boolean
 *              active:
 *                type: boolean
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      KeyPlainText:
 *        type: object
 *        required:
 *          - key
 *          - contentData
 *          - contentType
 *        properties:
 *          key:
 *            type: integer
 *          contentData:
 *            type: string
 *          contentType:
 *            type: string
 *            example: plain_text
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      KeyText:
 *        type: object
 *        required:
 *          - key
 *          - contentData
 *          - contentType
 *          - contentDataHtml
 *        properties:
 *          key:
 *            type: integer
 *          contentData:
 *            type: string
 *          contentType:
 *            type: string
 *            example: text
 *          contentDataHtml:
 *            type: string
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      KeyVideo:
 *        type: object
 *        required:
 *          - key
 *          - videoId
 *          - contentType
 *        properties:
 *          key:
 *            type: integer
 *          videoId:
 *            type: string
 *            format: uuid
 *          contentType:
 *            type: string
 *            example: video
 */



/**
* @swagger
*  components:
*    schemas:
*      KeyImage:
*        type: object
*        required:
*          - key
*          - contentData
*          - contentType
*          - contentDataHtml
*        properties:
*          key:
*            type: integer
*          contentData:
*            type: string
*            format: uuid
*          contentType:
*            type: string
*            example: image
*          contentDataHtml:
*            type: string
*            format: uuid
*/

/**
* @swagger
*  components:
*    schemas:
*      KeySlider:
*        type: object
*        required:
*          - key
*          - sliderId
*          - contentType
*        properties:
*          key:
*            type: integer
*          sliderId:
*            type: string
*            format: uuid
*          contentType:
*            type: string
*            example: image_slider
*/