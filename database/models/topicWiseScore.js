module.exports = (sequelize, type) => {
    return sequelize.define('topicWiseScore', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        userId: {
            type: type.INTEGER,
            allowNull: true,
        },
        testId: {
            type: type.INTEGER,
            allowNull: true,
        },
        completeScore: {
            type: type.INTEGER,
            allowNull: true
        },
        topicData: {
            type: type.JSONB,
            allowNull: true
        }
    }, {
        underscored: true,
        freezeTableName: false
    })
}

