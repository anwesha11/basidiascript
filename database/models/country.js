module.exports = (sequelize, type) => {
    const country = sequelize.define('country', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        name: {
            type: type.TEXT,
            allowNull: false
        },
        phone_code: {
            type: type.TEXT,
            allowNull: false
        }
    }, {
        underscored: true,
        freezeTableName: true
    })

    return country
}
