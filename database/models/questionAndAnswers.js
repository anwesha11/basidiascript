const _ = require('lodash')
module.exports = (sequelize, type) => {
    const questionAndAnswer = sequelize.define('questionAndAnswer', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        // question: {
        //     type: type.TEXT,
        //     allowNull: true
        // },  
        questionJson: {
            type: type.JSONB,
            allowNull: true
        },
        audioUrl: {
            type: type.TEXT,
            allowNull: true
        },
        answerJson: {
            type: type.JSONB,
            allowNull: true
        },
        // questionHtml: {
        //     type: type.TEXT,
        //     allowNull: true
        // },
        // answerHtml: {
        //     type: type.TEXT,
        //     allowNull: true,
        // },
        sortNumber: {
            type: type.INTEGER,
            allowNull: true
        },
        active: {
            type: type.BOOLEAN,
            allowNull: true,
            defaultValue: true
        },
        comingSoon: {
            type: type.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        type: {
            type: type.INTEGER,
            allowNull: false
        }
    }, {
        underscored: true,
        hooks: {
            afterCreate: (instance, options) => {
                ////console.log('instance-----------------', instance)
                sequelize.model('topic').findOne({ where: { id: instance.dataValues.topicId } }).then(val => {
                    ////console.log('topicVal', val)
                    let totalQnas = 1
                    if (val.dataValues.totalQnas) totalQnas = val.dataValues.totalQnas + totalQnas
                    sequelize.model('topic').update({ totalQnas }, { where: { id: instance.dataValues.topicId }, fields: ['totalQnas'] })
                })
                sequelize.model('subject').findOne({ where: { id: instance.dataValues.subjectId } }).then(val => {
                    let noOfQnas = 1
                    if (val && val.dataValues && val.dataValues.totalQna) {
                        noOfQnas = val.dataValues.totalQna + noOfQnas
                    }
                    sequelize.model('subject').update({ totalQna: noOfQnas }, { where: { id: instance.dataValues.subjectId }, fields: ['totalQna'] })
                })
            },

            beforeBulkUpdate: async (instance, options) => {
                ////console.log('Beforfe instance-----------------', instance)
                if (_.includes(instance.fields, 'subjectId')) {
                    const questionData = await sequelize.model('questionAndAnswer').findOne({ where: { uuidIdentifier: instance.where.uuidIdentifier } })
                    const subjectId = questionData.dataValues.subjectId
                    const subject = await sequelize.model('subject').findOne({ where: { id: subjectId } })
                    let count1 = subject.dataValues.totalQna
                    if (count1 && count1 > 0) {
                        count1 = count1 - 1
                        await sequelize.model('subject').update({ totalQna: count1 }, { where: { id: subjectId }, fields: ['totalQna'] })
                    }
                }
                if (_.includes(instance.fields, 'topicId')) {
                    const questionAndAnswerData = await sequelize.model('questionAndAnswer').findOne({ where: { uuidIdentifier: instance.where.uuidIdentifier } })

                    const topicId = questionAndAnswerData.dataValues.topicId

                    // update the questionAndAnswer count
                    const topic = await sequelize.model('topic').findOne({ where: { id: topicId } })
                    let count = topic.dataValues.totalQnas
                    if (count && count > 0) {
                        count = count - 1
                        await sequelize.model('topic').update({ totalQnas: count }, { where: { id: topicId }, fields: ['totalQnas'] })
                        //freezeTableName: true,

                    }
                }
            },
            afterBulkUpdate: async (instance, options) => {
                ////console.log('after instance-----------------', instance)
                if (_.includes(instance.fields, 'topicId')) {

                    const topic = await sequelize.model('topic').findOne({ where: { id: instance.attributes.topicId } })
                    let count = topic.dataValues.totalQnas
                    ////console.log(count)
                    if (count) count = count + 1
                    else count = 1
                    await sequelize.model('topic').update({ totalQnas: count }, { where: { id: instance.attributes.topicId }, fields: ['totalQnas'] })
                    if (_.includes(instance.fields, 'subjectId')) {

                        // update the subject count
                        const subject = await sequelize.model('subject').findOne({ where: { id: instance.attributes.subjectId } })
                        let count1 = subject.dataValues.totalQna

                        if (count1) count1 = count1 + 1
                        else count1 = 1
                        await sequelize.model('subject').update({ totalQna: count1 }, { where: { id: instance.attributes.subjectId }, fields: ['totalQna'] })

                    }
                }
            }
        }
    })

    // questionAndAnswer relationship
    questionAndAnswer.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    questionAndAnswer.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    questionAndAnswer.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })

    questionAndAnswer.belongsTo(sequelize.model('subject'),
        {
            foreignKey: {
                allowNull: false
            }
        })

    questionAndAnswer.belongsTo(sequelize.model('topic'),
        {
            foreignKey: {
                allowNull: false
            }
        })
    sequelize.model('topic').hasMany(questionAndAnswer)

    const qna_hashtag = sequelize.define('qna_hashtag', {},
        {
            underscored: true,
            freezeTableName: true,
            hooks: {
                afterCreate: async (instance, options) => {
                    sequelize.model('hashtag').findOne({ where: { id: instance.dataValues.hashtagId } }).then(val => {
                        let noOfQnas = 1
                        if (val && val.dataValues && val.dataValues.totalQna) {
                            noOfQnas = val.dataValues.totalQna + noOfQnas
                        }
                        sequelize.model('hashtag').update({ totalQna: noOfQnas }, { where: { id: instance.dataValues.hashtagId }, fields: ['totalQna'] })
                    })
                },
                beforeBulkDestroy: async (instance, options) => {
                    ////console.log(instance, 'before destroy')
                    const mcqHashTagData = await sequelize.model('qna_hashtag').findAll({ where: { questionAndAnswerId: instance.where.question_and_answer_id } })
                    if (mcqHashTagData.length > 0) {
                        mcqHashTagData.forEach(async val => {
                            let hashTagId = await sequelize.model('hashtag').findOne({
                                where: {
                                    id: val.hashtagId
                                }
                            })
                            let count1 = hashTagId.dataValues.totalQna
                            if (count1 && count1 > 0) {
                                count1 = count1 - 1
                                await sequelize.model('hashtag').update({ totalQna: count1 }, { where: { id: val.hashtagId }, fields: ['totalQna'] })
                            }

                        })

                    }
                }
            }
        })

    questionAndAnswer.belongsToMany(sequelize.model('hashtag'),
        {
            as: 'hashtags',
            through: qna_hashtag
        })

    const qna_reference = sequelize.define('qna_reference', {
        description: {
            type: type.TEXT,
            allowNull: true
        },
        sortNumber: {
            type: type.INTEGER,
            allowNull: true
        }
    }, {
        underscored: true,
        freezeTableName: true,
    })
    questionAndAnswer.belongsToMany(sequelize.model('reference'), {
        as: 'references',
        through: qna_reference
    })
    const qna_key = sequelize.define('qna_key', {

    }, {
        underscored: true,
        freezeTableName: true,
        hooks: {
            afterCreate: (instance, options) => {
                sequelize.model('key').findOne({ where: { id: instance.dataValues.keyId } }).then(val => {
                    let noOfQnas = 1
                    if (val && val.dataValues && val.dataValues.totalQnas) {
                        noOfQnas = val.dataValues.totalQnas + noOfQnas
                    }
                    sequelize.model('key').update({ totalQnas: noOfQnas }, { where: { id: instance.dataValues.keyId }, fields: ['totalQnas'] })
                })
            },
            beforeBulkDestroy: async (instance, options) => {
                ////console.log(instance, 'before destroy', options)
                const questionAndAnswerKeyData = await sequelize.model('qna_key').findAll({ where: { questionAndAnswerId: instance.where.question_and_answer_id } })
                // ////console.log(questionAndAnswerKeyData)


                if (questionAndAnswerKeyData.length > 0) {
                    questionAndAnswerKeyData.forEach(async keyData => {
                        // ////console.log(keyData);
                        const key = await sequelize.model('key').findOne({ where: { id: keyData.dataValues.keyId } })
                        // ////console.log(key)
                        let count = key.dataValues.totalQnas;
                        ////console.log(count);
                        if (count) {
                            count = count - 1
                        }
                        await sequelize.model('key').update({ totalQnas: count }, { where: { id: keyData.dataValues.keyId }, fields: ['totalQnas'] })

                    })

                }

            }


        }
    })
    questionAndAnswer.belongsToMany(sequelize.model('key'), {
        as: 'relatedKeys',
        through: qna_key
    })

    return questionAndAnswer
}

/**
 * @swagger
 *  components:
 *    schemas:
 *      Q&AWithoutId:
 *        type: object
 *        required:
 *          - questionJson
 *          - answerJson
 *          - subjectId
 *          - topicId
 *          - type
 *          - comingSoon
 *        properties:
 *          questionJson:
 *              type: string
 *          subjectId:
 *              type: string
 *              format: uuid
 *          topicId:
 *              type: string
 *              format: uuid
 *          answerJson:
 *              type: array
 *              items:
 *                 type: object
 *                 anyOf:
 *                     - $ref: '#/components/schemas/KeyText'
 *                     - $ref: '#/components/schemas/KeyPlainText'
 *                     - $ref: '#/components/schemas/KeyImage'
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          references:
 *            type: array
 *            items:
 *              type: object
 *              $ref: '#/components/schemas/ReferenceForInput'
 *          hashtags:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          active:
 *            type: boolean
 *          relatedKeys:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          type:
 *            type: string
 *            enum: [short, long, vshort, milestone]
 *          comingSoon:
 *            type: boolean
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      Q&AForUpdate:
 *        type: object
 *        properties:
 *          questionJson:
 *              type: string
 *          subjectId:
 *              type: string
 *              format: uuid
 *          topicId:
 *              type: string
 *              format: uuid
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          comingSoon:
 *            type: boolean
 *          answerJson:
 *              type: array
 *              items:
 *                 type: object
 *                 anyOf:
 *                     - $ref: '#/components/schemas/KeyText'
 *                     - $ref: '#/components/schemas/KeyPlainText'
 *                     - $ref: '#/components/schemas/KeyImage'
 *          references:
 *            type: array
 *            items:
 *              type: object
 *              $ref: '#/components/schemas/ReferenceForInput'
 *          hashtags:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          active:
 *            type: boolean
 *          relatedKeys:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          type:
 *            type: string
 *            enum: [short, long, vshort, milestone]
 */




/**
 * @swagger
 *  components:
 *    schemas:
 *      Q&AWithId:
 *        type: object
 *        properties:
 *          uuidIdentifier:
 *            type: uuid
 *          questionJson:
 *              type: string
 *          subjectId:
 *              type: string
 *              format: uuid
 *          topicId:
 *              type: string
 *              format: uuid
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          comingSoon:
 *            type: boolean
 *          answerJson:
 *              type: array
 *              items:
 *                 type: object
 *                 anyOf:
 *                     - $ref: '#/components/schemas/KeyText'
 *                     - $ref: '#/components/schemas/KeyPlainText'
 *                     - $ref: '#/components/schemas/KeyImage'
 *          createdBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          updatedBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          deletedBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          createdAt:
 *              type: string
 *              format: date-time
 *          updatedAt:
 *              type: string
 *              format: date-time
 *          deletedAt:
 *              type: string
 *              format: date-time
 *          references:
 *            type: array
 *            items:
 *              type: object
 *              $ref: '#/components/schemas/ReferenceForInput'
 *          hashtags:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          relatedKeys:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          type:
 *            type: string
 *            enum: [short, long, vshort, milestone]
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      Q&AForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              recover:
 *                type: boolean
 *              active:
 *                type: boolean
 *              subjectId:
 *                type: string
 *                format: uuid
 *              topicId:
 *                type: string
 *                format: uuid
 */

