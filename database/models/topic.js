const _ = require('lodash');
module.exports = (sequelize, type) => {
    const topic = sequelize.define('topic', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        name: {
            type: type.TEXT,
            allowNull: false,
            unique: false
        },
        description: {
            type: type.TEXT,
            allowNull: true,
        },
        sortNumber: {
            type: type.INTEGER,
            allowNull: true,
        },
        active: {
            type: type.BOOLEAN,
            allowNull: true,
            defaultValue: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        status: {
            type: type.INTEGER,
            allowNull: true
        },
        totalVideos: {
            type: type.INTEGER,
            allowNull: true
        },
        totalTest: {
            type: type.INTEGER,
            allowNull: true
        },
        totalStory: {
            type: type.INTEGER,
            allowNull: true
        },
        totalQnas: {
            type: type.INTEGER,
            allowNull: true
        },
        totalMcq: {
            type: type.INTEGER,
            allowNull: true
        },
        totalMccq: {
            type: type.INTEGER,
            allowNull: true
        },
        totalKeys: {
            type: type.INTEGER,
            allowNull: true
        },
        totalFitb: {
            type: type.INTEGER,
            allowNull: true
        },
        totalAtf: {
            type: type.INTEGER,
            allowNull: true
        },

    }, {
        underscored: true,
        freezeTableName: true,
        hooks: {
            afterCreate: (instance, options) => {
                ////console.log(instance, 'instance')
                sequelize.model('subject').findOne({ where: { id: instance.dataValues.subjectId } }).then(val => {
                    let noOfTopics = 1
                    if (val && val.dataValues && val.dataValues.totalTopics) {
                        noOfTopics = val.dataValues.totalTopics + noOfTopics
                    }
                    sequelize.model('subject').update({ totalTopics: noOfTopics }, { where: { id: instance.dataValues.subjectId }, fields: ['totalTopics'] })
                })

            },
            beforeBulkUpdate: async (instance, options) => {
                ////console.log('Beforfe instance-----------------', instance)
                if (_.includes(instance.fields, 'subjectId')) {
                    const topicData = await sequelize.model('topic').findOne({ where: { uuidIdentifier: instance.where.uuidIdentifier } })
                    const subjectId = topicData.dataValues.subjectId
                    const subject = await sequelize.model('subject').findOne({ where: { id: subjectId } })
                    let count1 = subject.dataValues.totalTopics
                    if (count1 && count1 > 0) {
                        count1 = count1 - 1
                        await sequelize.model('subject').update({ totalTopics: count1 }, { where: { id: subjectId }, fields: ['totalTopics'] })
                    }
                }
            },
            afterBulkUpdate: async (instance, options) => {
                ////console.log('after instance-----------------', instance)
                if (_.includes(instance.fields, 'subjectId')) {

                    // update the subject count
                    const subject = await sequelize.model('subject').findOne({ where: { id: instance.attributes.subjectId } })
                    let count1 = subject.dataValues.totalTopics

                    if (count1) count1 = count1 + 1
                    else count1 = 1
                    await sequelize.model('subject').update({ totalTopics: count1 }, { where: { id: instance.attributes.subjectId }, fields: ['totalTopics'] })

                }
            }
        }
    })

    // topic relationship
    topic.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    topic.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    topic.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })
    topic.belongsTo(sequelize.model('subject'),
        {
            foreignKey: {
                allowNull: false
            }
        })
    sequelize.model('subject').hasMany(topic)
    topic.belongsTo(sequelize.model('classes'),
        {
            as: 'class',
            foreignKey: {
                name: 'classId',
                allowNull: false
            }
        })
    topic.belongsTo(sequelize.model('filestore'),
        {
            as: 'thumbnailUrl',
            foreignKey: {
                name: 'thumbnail',
                allowNull: true
            }
        })


    const topics_super_topic = sequelize.define('topics_super_topic', {

    }, {
        underscored: true,
        freezeTableName: true,
        hooks: {
            afterCreate: (instance, options) => {

                sequelize.model('superTopic').findOne({ where: { id: instance.dataValues.superTopicId } }).then(val => {
                    let noOfTopic = 1
                    if (val && val.dataValues && val.dataValues.totalTopic) {
                        noOfTopic = val.dataValues.totalTopic + noOfTopic
                    }
                    sequelize.model('superTopic').update({ totalTopic: noOfTopic }, { where: { id: instance.dataValues.superTopicId }, fields: ['totalTopic'] })
                })
            }
        }
    })

    topic.belongsToMany(sequelize.model('superTopic'),
        {
            as: 'parentTopics',
            through: topics_super_topic,
            allowNull: false
        })
    sequelize.model('superTopic').belongsToMany(topic,
        {
            as: 'childTopics',
            through: topics_super_topic,
            allowNull: false
        })
    return topic
}

/**
 * @swagger
 *  components:
 *    schemas:
 *      TopicWithoutId:
 *        type: object
 *        required:
 *          - name
 *          - sortNumber
 *          - classId
 *          - subjectId
 *          - active
 *          - isChildTopic
 *          - superTopicId
 *          - status
 *        properties:
 *          name:
 *            type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          classes:
 *            type: string
 *            format: uuid
 *          subjectId:
 *            type: string
 *            format: uuid
 *          active:
 *            type: boolean
 *          thumbnail:
 *            type: string
 *            format: uuid
 *          isChildTopic:
 *            type: boolean
 *          superTopicId:
 *            type: string
 *            format: uuid
 *          status:
 *            type: string
 *            enums: [free, locked]
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      TopicForUpdate:
 *        type: object
 *        properties:
 *          name:
 *            type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          classId:
 *            type: string
 *            format: uuid
 *          subjectId:
 *            type: string
 *            format: uuid
 *          active:
 *            type: boolean
 *          thumbnail:
 *            type: string
 *            format: uuid
 *          status:
 *            type: string
 *            enums: [free, locked]
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      TopicForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              recover:
 *                  type: boolean
 *              active:
 *                  type: boolean
 */

/**
* @swagger
*  components:
*    schemas:
*      TopicWithId:
*        type: object
*        properties:
*          uuidIdentifier:
*            type: uuid
*          name:
*            type: string
*          sortNumber:
*            type: integer
*            format: int32
*          classId:
*            type: string
*            format: uuid
*          subjectId:
*            type: string
*            format: uuid
*          createdBy:
*            type: object
*            $ref: '#/components/schemas/CreatedBy'
*          updatedBy:
*            type: object
*            $ref: '#/components/schemas/CreatedBy'
*          deletedBy:
*            type: object
*            $ref: '#/components/schemas/CreatedBy'
*          thumbnailUrl:
*            type: object
*            $ref: '#/components/schemas/ImageUrl'
*          createdAt:
*            type: string
*            format: date
*          updatedAt:
*            type: string
*            format: date
*          deletedAt:
*            type: string
*            format: date
*          status:
*            type: string
*            enums: [free,locked]
*/