const _ = require('lodash')
module.exports = (sequelize, type) => {
    const test = sequelize.define('test', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        name: {
            type: type.TEXT,
            allowNull: false
        },
        sortNumber: {
            type: type.INTEGER,
            allowNull: true,
        },
        mcqCount: {
            type: type.INTEGER,
            allowNull: true,
            defaultValue: 0
        },
        resultDate: {
            type: type.DATEONLY,
            allowNull: true
        },
        duration: {
            type: type.INTEGER,
            allowNull: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        active: {
            type: type.BOOLEAN,
            allowNull: false,
            defaultValue: true
        },
        status: {
            type: type.INTEGER,
            allowNull: false
        },
        type: {
            type: type.INTEGER,
            allowNull: false,
            validate: {
                min: 0,
                max: 1
            }
        },
        isFeatured: {
            type: type.BOOLEAN,
            allowNull: true,
            defaultValue: false
        },
        shortname: {
            type: type.STRING,
            allowNull: true
        },
    }, {
        underscored: true,
        freezeTableName: true
    })

    // test relationship
    test.belongsTo(sequelize.model('subject'),
        {
            foreignKey: {
                allowNull: true
            }
        })



    test.belongsTo(sequelize.model('topic'),
        {
            foreignKey: {
                allowNull: true
            }
        })
    test.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    test.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    test.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })
    test.belongsTo(sequelize.model('classes'),
        {
            as: 'class',
            foreignKey: {
                name: 'classId',
                allowNull: true
            }
        })
    test.belongsTo(sequelize.model('filestore'),
        {
            as: 'thumbnailUrl',
            foreignKey: {
                name: 'thumbnail',
                allowNull: true
            }
        })
    return test
}

/**
 * @swagger
 *  components:
 *    schemas:
 *      TestWithoutId:
 *        type: object
 *        required:
 *          - name
 *          - type
 *          - status
 *          - mcqCount
 *          - startDate
 *          - endDate
 *          - sortNumber
 *          - classId
 *        properties:
 *          name:
 *            type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          mcqCount:
 *            type: integer
 *            format: int32
 *          resultDate:
 *            type: string
 *            format: date
 *          status:
 *            type: string
 *            enum: [Paid, Free]
 *          active:
 *            type: boolean
 *          type:
 *            type: string
 *            enum: [mockTest, qBank]
 *          subjectId:
 *            type: string
 *            format: uuid
 *          topicId:
 *            type: string
 *            format: uuid
 *          classId:
 *            type: string
 *            format: uuid
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      TestForUpdate:
 *        type: object
 *        properties:
 *          name:
 *            type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          mcqCount:
 *            type: integer
 *            format: int32
 *          resultDate:
 *            type: string
 *            format: date
 *          status:
 *            type: string
 *            enum: [Paid, Free]
 *          active:
 *            type: boolean
 *          type:
 *            type: string
 *            enum: [mockTest, qBank]
 *          subjectId:
 *            type: string
 *            format: uuid
 *          topicId:
 *            type: string
 *            format: uuid
 *          classId:
 *            type: string
 *            format: uuid
 */




/**
 * @swagger
 *  components:
 *    schemas:
 *      TestWithId:
 *        type: object
 *        properties:
 *          uuidIdentifier:
 *            type: uuid
 *          name:
 *            type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          mcqCount:
 *            type: integer
 *            format: int32
 *          resultDate:
 *            type: string
 *            format: date
 *          status:
 *            type: string
 *            enum: [Paid, Free]
 *          active:
 *            type: boolean
 *          type:
 *            type: string
 *            enum: [mockTest, qBank]
 *          subjectId:
 *            type: string
 *            format: uuid
 *          topicId:
 *            type: string
 *            format: uuid
 *          createdBy:
 *            type: object
 *            $ref: '#/components/schemas/CreatedBy'
 *          updatedBy:
 *            type: object
 *            $ref: '#/components/schemas/CreatedBy'
 *          deletedBy:
 *            type: object
 *            $ref: '#/components/schemas/CreatedBy'
 *          createdAt:
 *            type: string
 *            format: date
 *          updatedAt:
 *            type: string
 *            format: date
 *          deletedAt:
 *            type: string
 *            format: date
 *          classId:
 *            type: string
 *            format: uuid
 */

/**
* @swagger
*  components:
*    schemas:
*      TestForBulkUpdate:
*        type: object
*        required:
*          - ids
*          - updates
*        properties:
*          ids:
*            type: array
*            items:
*              type: string
*              format: uuid
*          updates:
*            type: object
*            properties:
*              recover:
*                type: boolean
*              active:
*                type: boolean
*/

/**
* @swagger
*  components:
*    schemas:
*      TestForBulkUpdateSection:
*        type: object
*        required:
*          - testId
*          - mcqIds
*          - section
*        properties:
*          mcqIds:
*            type: array
*            items:
*              type: string
*              format: uuid
*          testId:
*            type: string
*            format: uuid
*          section:
*            type: string
*            enum: [A,B]

*/

/**
* @swagger
*  components:
*    schemas:
*      TestSectionWithoutId:
*        type: object
*        required:
*          - testId
*          - mcqIds
*          - section
*          - sortNumber
*        properties:
*          mcqId:
*            type: string
*            format: uuid
*          testId:
*            type: string
*            format: uuid
*          sortNumber:
*            type: integer
*            format: int32
*          section:
*            type: string
*            enum: [A,B]

*/



