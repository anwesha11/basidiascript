module.exports = (sequelize, type) => {
    const hashtag = sequelize.define('hashtag', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        title: {
            type: type.TEXT,
            allowNull: false
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        active: {
            type: type.BOOLEAN,
            allowNull: true,
            defaultValue: true
        },
        totalMcq: {
            type: type.INTEGER,
            allowNull: true
        },
        totalQna: {
            type: type.INTEGER,
            allowNull: true
        },
        totalMccq: {
            type: type.INTEGER,
            allowNull: true
        },
        totalAtf: {
            type: type.INTEGER,
            allowNull: true
        },
        totalFitb: {
            type: type.INTEGER,
            allowNull: true
        },
    }, {
        underscored: true,
        freezeTableName: true,
    })

    // hashtag relationship
    hashtag.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false
            }
        })
    hashtag.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })
    hashtag.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })
    return hashtag
}

/**
 * @swagger
 *  components:
 *    schemas:
 *      HashtagWithoutId:
 *        type: object
 *        required:
 *          - title
 *          - active
 *        properties:
 *          title:
 *            type: string
 *          active:
 *            type: boolean
 */

/**
* @swagger
*  components:
*    schemas:
*      HashtagForUpdate:
*        type: object
*        properties:
*          title:
*            type: string
*          active:
*            type: boolean
*/

/**
* @swagger
*  components:
*    schemas:
*      HashtagWithId:
*        type: object
*        properties:
*          uuidIdentifier:
*            type: string
*            format: uuid
*          title:
*            type: string
*          creationUser:
*            type: object
*            $ref: '#/components/schemas/CreatedBy'
*          updationUser:
*            type: object
*            $ref: '#/components/schemas/CreatedBy'
*          deletionUser:
*            type: object
*            $ref: '#/components/schemas/CreatedBy'
*          active:
*            type: boolean
*          createdAt:
*            type: string
*            format: date-time
*          updatedAt:
*            type: string
*            format: date-time
*          deletedAt:
*            type: string
*            format: date-time
*/


/**
 * @swagger
 *  components:
 *    schemas:
 *      HashtagForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              recover:
 *                  type: boolean
 *              active:
 *                type: boolean
 */