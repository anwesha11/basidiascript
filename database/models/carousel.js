module.exports = (sequelize, type) => {
    const carousel = sequelize.define('carousel', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        title: {
            type: type.TEXT,
            allowNull: false
        },
        subTitle: {
            type: type.TEXT,
            allowNull: false
        },
        type: {
            type: type.INTEGER,
            allowNull: false
        },
        link: {
            type: type.TEXT,
            allowNull: true
        },
        contentType: {
            type: type.INTEGER,
            allowNull: true
        },
        contentId: {
            type: type.TEXT,
            allowNull: true
        },
        typeOfCarousel: {
            type: type.INTEGER,
            allowNull: false
        },
        audience: {
            type: type.INTEGER,
            allowNull: true
        },
        active: {
            type: type.BOOLEAN,
            allowNull: false,
            defaultValue: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        sortNumber: {
            type: type.INTEGER,
            allowNull: true
        }
    }, {
        underscored: true,
        freezeTableName: true
    })

    carousel.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    carousel.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    carousel.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })

    // carousel.belongsTo(sequelize.model('filestore'),
    //     {
    //         as: 'imageUrl',
    //         foreignKey: {
    //             name: 'image',
    //             allowNull: true
    //         }
    //     })
    carousel.belongsTo(sequelize.model('classes'),
        {
            as: 'class',
            foreignKey: {
                name: 'classId',
                allowNull: true
            }
        })
    carousel.belongsTo(sequelize.model('subject'),
        {
            foreignKey: {
                allowNull: true
            }
        })
    carousel.belongsTo(sequelize.model('topic'),
        {
            foreignKey: {
                allowNull: true
            }
        })
    carousel.belongsTo(sequelize.model('filestore'),
        {
            as: 'thumbnailUrl',
            foreignKey: {
                name: 'thumbnail',
                allowNull: true
            }
        })
    return carousel
}


/**
 * @swagger
 *  components:
 *    schemas:
 *      CarouselWithoutId:
 *        type: object
 *        required:
 *          - title
 *          - subTitle
 *          - type
 *          - link
 *          - contentType
 *          - active
 *          - classId
 *          - topicId
 *          - subjectId
 *          - contentId
 *          - typeOfCarousel
 *        properties:
 *          title:
 *            type: string
 *          subTitle:
 *            type: string
 *          type:
 *            type: string
 *            enum: [video, image, json]
 *          link:
 *            type: string
 *          contentType:
 *            type: string
 *            enum: [ video, mockTest, qBank, url]
 *          typeOfCarousel:
 *            type: string
 *            enum: [ normal, subscription]
 *          sortNumber:
 *            type: integer
 *          active:
 *            type: boolean
 *          classId:
 *            type: string
 *            format: uuid
 *          topicId:
 *            type: string
 *            format: uuid
 *          subjectId:
 *            type: string
 *            format: uuid
 *          contentId:
 *            type: string
 *            format: uuid
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      CarouselForUpdate:
 *        type: object
 *        properties:
 *          title:
 *            type: string
 *          subTitle:
 *            type: string
 *          type:
 *            type: string
 *            enum: [video, image, json]
 *          link:
 *            type: string
 *          contentType:
 *            type: string
 *            enum: [ video, mockTest, qBank, url, qna, key, mcq, lesson ]
 *          sortNumber:
 *            type: integer
 *          active:
 *            type: boolean
 *          classId:
 *            type: string
 *            format: uuid
 *          topicId:
 *            type: string
 *            format: uuid
 *          subjectId:
 *            type: string
 *            format: uuid
 *          contentId:
 *            type: string
 *            format: uuid
 *          typeOfCarousel:
 *            type: string
 *            enum: [ normal, subscription]
 */






/**
 * @swagger
 *  components:
 *    schemas:
 *      CarouselWithId:
 *        type: object
 *        properties:
 *          uuidIdentifier:
 *            type: uuid
 *          title:
 *            type: string
 *          subTitle:
 *            type: string
 *          type:
 *            type: string
 *            enum: [video, image, json]
 *          link:
 *            type: string
 *          contentType:
 *            type: string
 *            enum: [ video, mockTest, qBank, url, qna,key, mcq, lesson ]
 *          sortNumber:
 *            type: integer
 *          active:
 *            type: boolean
 *          topicId:
 *            type: string
 *            format: uuid
 *          subjectId:
 *            type: string
 *            format: uuid
 *          contentId:
 *            type: string
 *            format: uuid
 *          createdBy:
 *            type: string
 *            $ref: '#/components/schemas/CreatedBy'
 *          updatedBy:
 *            type: string
 *            $ref: '#/components/schemas/CreatedBy'
 *          deletedBy:
 *            type: string
 *            $ref: '#/components/schemas/CreatedBy'
 *          deletedAt:
 *            type: string
 *            format: date
 *          updatedAt:
 *            type: string
 *            format: date
 *          createdAt:
 *            type: string
 *            format: date
 *          classId:
 *            type: string
 *            format: uuid
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      CarouselForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              recover:
 *                type: boolean
 *              active:
 *                type: boolean
 */