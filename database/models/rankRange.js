const moment = require('moment')
module.exports = (sequelize, type) => {
    const rankRange = sequelize.define('rankRange',{
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        startRank:{
            type:type.STRING,
            allowNull: true
        },
        endRank:{
            type:type.STRING,
            allowNull: true
        },
        startScore:{
            type:type.STRING,
            allowNull: true
        },
        endScore:{
            type:type.STRING,
            allowNull: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        }
    },{
        underscored: true,
        freezeTableName: false
    })
    rankRange.belongsTo(sequelize.model('user'),
    {
        as:'creationUser',
        foreignKey:{
            name:'createdBy',
            allowNull: false
        }

    })
    rankRange.belongsTo(sequelize.model('user'),
    {
        as:'updationUser',
        foreignKey:{
            name:'updatedBy',
            allowNull: false
        }

    })
    rankRange.belongsTo(sequelize.model('user'),
    {
        as:'deletionUser',
        foreignKey:{
            name:'deletedBy',
            allowNull: true
        }

    })
    rankRange.belongsTo(sequelize.model('test'),{
        foreignKey: {
            allowNull: false
        }
    })

    return rankRange;
}


/**
 * @swagger
 *  components:
 *    schemas:
 *      rankRangeWithoutID: 
 *        type: object
 *        required:
 *          - startRank
 *          - endRank
 *          - startScore
 *          - endScore
 *          - testId
 *        properties:
 *          startRank:
 *              type: string
 *              format: byte
 *          endRank:
 *              type: string
 *              format: byte
 *          startScore:
 *              type: string
 *              format: byte
 *          endScore:
 *              type: string
 *              format: byte
 *          testId:
 *              type: string
 *              format: uuid
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      rankRangeForUpdate:
 *        type: object
 *        properties:
 *          startRank:
 *              type: string
 *              format: byte
 *          endRank:
 *              type: string
 *              format: byte
 *          startScore:
 *              type: string
 *              format: byte
 *          endScore:
 *              type: string
 *              format: byte
 *          testId:
 *              type: string
 *              format: uuid
 */




/**
 * @swagger
 *  components:
 *    schemas:
 *      rankRangeWithID:
 *        type: object
 *        properties:
 *          startRank:
 *              type: string
 *              format: byte
 *          endRank:
 *              type: string
 *              format: byte
 *          startScore:
 *              type: string
 *              format: byte
 *          endScore:
 *              type: string
 *              format: byte
 *          testId:
 *              type: integer
 *              format: uuid
 *          createdBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          updatedBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          deletedBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 */