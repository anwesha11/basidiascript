module.exports = (sequelize, type) => {
    const coupon = sequelize.define('coupon', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        active: {
            type: type.BOOLEAN,
            allowNull: false,
            defaultValue: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        code: {
            type: type.STRING,
            allowNull: false
        },
        offAmount: {
            type: type.FLOAT,
            allowNull: true
        },
        offPercentage: {
            type: type.FLOAT,
            allowNull: true
        },
        limit: {
            type: type.INTEGER,
            allowNull: false
        },
        isUsed: {
            type: type.INTEGER,
            allowNull: false,
            defaultValue: 0
        }
    }, {
        underscored: true,
        freezeTableName: true,
    })

    // coupon relationship
    coupon.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    coupon.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    coupon.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })
    coupon.belongsTo(sequelize.model('subscription'),
        {
            foreignKey: {
                allowNull: false
            }
        })
    coupon.belongsTo(sequelize.model('classes'),
        {
            as: 'class',
            foreignKey: {
                name: 'classId',
                allowNull: false
            }
        })
    return coupon
}

/**
 * @swagger
 *  components:
 *    schemas:
 *      CouponWithoutId:
 *        type: object
 *        required:
 *          - code
 *          - offAmount
 *          - offPercentage
 *          - limit
 *          - subscriptionId
 *          - active
 *          - classId
 *        properties:
 *          code:
 *            type: string
 *          limit:
 *            type: integer
 *            format: int32
 *          offAmount:
 *            type: number
 *            format: float
 *          offPercentage:
 *            type: number
 *            format: float
 *          subscriptionId:
 *            type: string
 *            format: uuid
 *          active:
 *            type: boolean
 *          classId:
 *            type: string
 *            format: uuid
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      CouponForUpdate:
 *        type: object
 *        properties:
 *          code:
 *            type: string
 *          limit:
 *            type: integer
 *            format: int32
 *          offAmount:
 *            type: number
 *            format: float
 *          offPercentage:
 *            type: number
 *            format: float
 *          subscriptionId:
 *            type: string
 *            format: uuid
 *          active:
 *            type: boolean
 *          classId:
 *            type: string
 *            format: uuid
 */



/**
* @swagger
*  components:
*    schemas:
*      CouponWithId:
*        type: object
*        properties:
*          uuidIdentifier:
*            type: uuid
*          code:
*            type: string
*          offAmount:
*            type: integer
*            format: int32
*          offPercentage:
*            type: integer
*            format: int32
*          limit:
*            type: integer
*            format: int32
*          subscriptionId:
*            type: string
*            format: uuid
*          createdBy:
*            type: object
*            $ref: '#/components/schemas/CreatedBy'
*          updatedBy:
*            type: object
*            $ref: '#/components/schemas/CreatedBy'
*          deletedBy:
*            type: object
*            $ref: '#/components/schemas/CreatedBy'
*          createdAt:
*            type: string
*            format: date
*          updatedAt:
*            type: string
*            format: date
*          deletedAt:
*            type: string
*            format: date
*          active:
*            type: boolean
*          classId:
*            type: string
*            format: uuid
*/


/**
 * @swagger
 *  components:
 *    schemas:
 *      CouponForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              recover:
*                 type: boolean
 *              active:
 *                type: boolean
 *
 */