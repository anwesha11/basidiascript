module.exports = (sequelize, type) => {
    const classes = sequelize.define('classes', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        name: {
            type: type.STRING,
            allowNull: false,
            unique: {
                msg: 'name is already in use'
            }
        },
        description: {
            type: type.TEXT,
            allowNull: false
        },
        code: {
            type: type.INTEGER,
            allowNull: false,
            unique: {
                msg: 'code is already in use'
            }
        },
        sortNumber: {
            type: type.INTEGER,
            allowNull: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
    }, {
        underscored: true,
        freezeTableName: true
    });

    classes.belongsTo(sequelize.model('app'), {
        foreignKey: {
            allowNull: true
        }
    })

    return classes;
};


/**
 * @swagger
 *  components:
 *    schemas:
 *      ClassesWithoutId:
 *        type: object
 *        required:
 *          - name
 *          - description
 *          - sortNumber
 *        properties:
 *          name:
 *            type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          description:
 *            type: string
 *          subjectIds:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      ClassesForUpdate:
 *        type: object
 *        properties:
 *          name:
 *            type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          description:
 *            type: string
 */
/**
 * @swagger
 *  components:
 *    schemas:
 *      AddSubjectToClass:
 *        type: object
 *        properties:
 *          subjectIds:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      ClassesWithId:
 *        type: object
 *        properties:
 *          uuidIdentifier:
 *            type: string
 *            format: uuid
 *          name:
 *            type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          description:
 *            type: string
 *          createdAt:
 *            type: string
 *            format: date-time
 *          updatedAt:
 *            type: string
 *            format: date-time
 *          deletedAt:
 *            type: string
 *            format: date-time
 *          code:
 *            type: integer
 *            format: int32
 */

