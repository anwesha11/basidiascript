module.exports = (sequelize, type) => {
    const privilege = sequelize.define('privilege', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        name: {
            type: type.STRING,
            allowNull: false,
            unique:{
                msg: 'Name is already in use'
            }
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        description: {
            type: type.STRING,
            allowNull: true
        }
    }, {
        underscored: true,
        freezeTableName: true,
    })

    // privilege relationship
    privilege.belongsTo(sequelize.model('classes'), {
        foreignKey: {
            name: 'classId',
            allowNull: true
        }
    })
    privilege.belongsTo(sequelize.model('subject'), {
        foreignKey: {
            name: 'subjectId',
            allowNull: true
        }
    })
    privilege.belongsTo(sequelize.model('modules'), {
        foreignKey: {
            name: 'moduleId',
            allowNull: true
        }
    })
    sequelize.model('subject').hasMany(privilege)
    sequelize.model('modules').hasMany(privilege)

    privilege.belongsToMany(sequelize.model('role'), {
        as: 'roles',
        through: 'role_privilege',
        allowNull: true
    })
    sequelize.model('role').belongsToMany(privilege, {
        as: 'privileges',
        through: 'role_privilege',
        allowNull: true
    })
    return privilege
}

/**
 * @swagger
 *  components:
 *    schemas:
 *      PrivilegeWithoutId:
 *        type: object
 *        required:
 *          - name
 *          - description
 *        properties:
 *          name:
 *            type: string
 *          description:
 *            type: string
 */


/**
* @swagger
*  components:
*    schemas:
*      PrivilegeForUpdate:
*        type: object
*        properties:
*          name:
*            type: string
*          description:
*            type: string
*/

/**
* @swagger
*  components:
*    schemas:
*      PrivilegeWithId:
*        type: object
*        properties:
*          uuidIdentifier:
*            type: string
*            format: uuid
*          name:
*            type: string
*          description:
*            type: string
*          createdAt:
*            type: string
*            format: date-time
*          updatedAt:
*            type: string
*            format: date-time
*          deletedAt:
*            type: string
*            format: date-time
*/

/**
 * @swagger
 *  components:
 *    schemas:
 *      Modules:
 *        type: object
 *        required:
 *          - name
 *          - code
 *        properties:
 *          name:
 *            type: string
 *          code:
 *            type: string
 */
