module.exports = (sequelize, type) => {
    const token = sequelize.define('token', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        firebaseToken: {
            type: type.TEXT,
            allowNull: true
        },
        refreshToken: {
            type: type.TEXT,
            allowNull: true
        },
        deviceType: {
            type: type.TEXT,
            allowNull: true
        },
        deviceLocation: {
            type: type.TEXT,
            allowNull: true
        },
        deviceIP: {
            type: type.TEXT,
            allowNull: true
        },
        devicePlatform: {
            type: type.TEXT,
            allowNull: true
        }
    }, {
        underscored: true,
        freezeTableName: true,
    })

    // token relationship
    token.belongsTo(sequelize.model('user'),
        {
            foreignKey: {
                allowNull: true
            }
        })
    token.belongsTo(sequelize.model('app'),
        {
            foreignKey: {
                allowNull: true
            }
        })
    return token
}
