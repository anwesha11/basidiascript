module.exports = (sequelize, type) => {
    const subscriptionImage = sequelize.define('subscriptionImage', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        images:{
            type: type.JSONB,
            allowNull: false
        },
        active: {
            type: type.BOOLEAN,
            allowNull: true,
            defaultValue: true
        },
        type: {
            type: type.INTEGER,
            allowNull: true,
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
    }, {
        underscored: true,
        freezeTableName: false,
    });

    subscriptionImage.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    subscriptionImage.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    subscriptionImage.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })

    subscriptionImage.belongsTo(sequelize.model('classes'),
        {
            as: 'class',
            foreignKey: {
                name: 'classId',
                allowNull: false
            }
        })
    return subscriptionImage;
};

/**
 * @swagger
 *  components:
 *    schemas:
 *      SubscriptionImagesWithoutId:
 *        type: object
 *        required:
 *          - classId
 *          - images
 *          - active
 *        properties:
 *          images:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          classId:
 *            type: string
 *            format: uuid
 *          active:
 *            type: boolean
 */

