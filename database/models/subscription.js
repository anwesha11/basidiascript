module.exports = (sequelize, type) => {
    const subscription = sequelize.define('subscription', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        active: {
            type: type.BOOLEAN,
            allowNull: false,
            defaultValue: true
        },
        title: {
            type: type.STRING,
            allowNull: false
        },
        description: {
            type: type.STRING,
            allowNull: true
        },
        amount: {
            type: type.FLOAT,
            allowNull: false
        },
        duration: {
            type: type.INTEGER,
            allowNull: false
        },
        components: {
            type: type.STRING,
            allowNull: false
        },
        oldPrice: {
            type: type.INTEGER,
            allowNull: true
        },
        line1: {
            type: type.STRING,
            allowNull: false
        },
        line2: {
            type: type.STRING,
            allowNull: false
        },
        line3: {
            type: type.STRING,
            allowNull: true
        },
        line4: {
            type: type.STRING,
            allowNull: true
        },
        line5: {
            type: type.STRING,
            allowNull: true
        },
        savePercentage: {
            type: type.FLOAT,
            allowNull: true
        },
        startDate: {
            type: type.DATEONLY,
            allowNull: false
        },
        endDate: {
            type: type.DATEONLY,
            allowNull: false
        },
    }, {
        underscored: true,
        freezeTableName: true,
    });

    subscription.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    subscription.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    subscription.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })

    subscription.belongsTo(sequelize.model('classes'),
        {
            as: 'class',
            foreignKey: {
                name: 'classId',
                allowNull: false
            }
        })
    return subscription;
};

/**
 * @swagger
 *  components:
 *    schemas:
 *      CheckCoupon:
 *        type: object
 *        required:
 *          - subscriptionId
 *          - coupon
 *        properties:
 *          subscriptionId:
 *            type: string
 *            format: uuid
 *          coupon:
 *            type: string
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      SubscriptionWithoutId:
 *        type: object
 *        required:
 *          - title
 *          - description
 *          - amount
 *          - duration
 *          - components
 *          - line1
 *          - classId
 *        properties:
 *          title:
 *            type: string
 *          description:
 *            type: string
 *          active:
 *            type: boolean
 *          amount:
 *            type: number
 *            format: float
 *          components:
 *            type: array
 *            items:
 *              type: string
 *              enums: [learn, test, video]
 *          duration:
 *            type: integer
 *          oldPrice:
 *            type: number
 *            format: float
 *          line1:
 *            type: string
 *          line2:
 *            type: string
 *          line3:
 *            type: string
 *          line4:
 *            type: string
 *          line5:
 *            type: string
 *          startDate:
 *            type: string
 *            format: date
 *          endDate:
 *            type: string
 *            format: date
 *          savePercentage:
 *            type: number
 *            format: float
 *          classId:
 *            type: string
 *            format: uuid
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      SubscriptionForUpdate:
 *        type: object
 *        properties:
 *          title:
 *            type: string
 *          description:
 *            type: string
 *          active:
 *            type: boolean
 *          amount:
 *            type: number
 *            format: float
 *          components:
 *            type: array
 *            items:
 *              type: string
 *              enums: [learn, test, video]
 *          duration:
 *            type: integer
 *          oldPrice:
 *            type: number
 *            format: float
 *          line1:
 *            type: string
 *          line2:
 *            type: string
 *          line3:
 *            type: string
 *          line4:
 *            type: string
 *          line5:
 *            type: string
 *          startDate:
 *            type: string
 *            format: date
 *          endDate:
 *            type: string
 *            format: date
 *          savePercentage:
 *            type: number
 *            format: float
 *          classId:
 *            type: string
 *            format: uuid
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      SubscriptionWithId:
 *        type: object
 *        properties:
 *          uuidIdentifier:
 *            type: string
 *            format: uuid
 *          title:
 *            type: string
 *          description:
 *            type: string
 *          active:
 *            type: boolean
 *          amount:
 *            type: number
 *            format: float
 *          components:
 *            type: array
 *            items:
 *              type: string
 *              enums: [learn, test, video]
 *          duration:
 *            type: integer
 *          oldPrice:
 *            type: number
 *            format: float
 *          line1:
 *            type: string
 *          line2:
 *            type: string
 *          line3:
 *            type: string
 *          line4:
 *            type: string
 *          line5:
 *            type: string
 *          startDate:
 *            type: string
 *            format: date
 *          endDate:
 *            type: string
 *            format: date
 *          savePercentage:
 *            type: number
 *            format: float
 *          createdBy:
 *            type: string
 *            format: uuid
 *          updatedBy:
 *            type: string
 *            format: uuid
 *          deletedBy:
 *            type: string
 *            format: uuid
 *          createdAt:
 *            type: string
 *            format: date
 *          updatedAt:
 *            type: string
 *            format: date
 *          deletedAt:
 *            type: string
 *            format: date
 *          classId:
 *            type: string
 *            format: uuid
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      SubscriptionForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              recover:
*                 type: boolean
 *              active:
 *                type: boolean
 *              subjectId:
 *                type: string
 *                format: uuid
 *              topicId:
 *                type: string
 *                format: uuid
 */