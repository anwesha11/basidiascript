const _ = require('lodash')
module.exports = (sequelize, type) => {

    const mccq = sequelize.define('mccq', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        question: {
            type: type.TEXT,
            allowNull: false
        },
        options: {
            type: type.JSONB,
            allowNull: false,
        },
        questionHtml: {
            type: type.TEXT,
            allowNull: false
        },
        explanation: {
            type: type.JSONB,
            allowNull: true,
        },
        active: {
            type: type.BOOLEAN,
            allowNull: true,
            defaultValue: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        sortNumber: {
            type: type.INTEGER,
            allowNull: true
        },
        type: {
            type: type.INTEGER,
            allowNull: false
        }
    }, {
        underscored: true,
        freezeTableName: true,
        hooks: {
            afterCreate: (instance, options) => {
                ////console.log('instance-----------------', instance)
                sequelize.model('topic').findOne({ where: { id: instance.dataValues.topicId } }).then(val => {
                    ////console.log('topicVal', val)
                    let totalMccq = 1
                    if (val.dataValues.totalMccq) totalMccq = val.dataValues.totalMccq + totalMccq
                    sequelize.model('topic').update({ totalMccq }, { where: { id: instance.dataValues.topicId }, fields: ['totalMccq'] })
                })
                sequelize.model('subject').findOne({ where: { id: instance.dataValues.subjectId } }).then(val => {
                    let noOfMccq = 1
                    if (val && val.dataValues && val.dataValues.totalMccq) {
                        noOfMccq = val.dataValues.totalMccq + noOfMccq
                    }
                    sequelize.model('subject').update({ totalMccq: noOfMccq }, { where: { id: instance.dataValues.subjectId }, fields: ['totalMccq'] })
                })
            },
            beforeBulkUpdate: async (instance, options) => {
                ////console.log('Beforfe instance-----------------', instance)

                if (_.includes(instance.fields, 'topicId')) {
                    const mccqData = await sequelize.model('mccq').findOne({ where: { uuidIdentifier: instance.where.uuidIdentifier } })

                    const topicId = mccqData.dataValues.topicId

                    // update the mccq count
                    const topic = await sequelize.model('topic').findOne({ where: { id: topicId } })
                    let count = topic.dataValues.totalMccq
                    if (count && count > 0) {
                        count = count - 1
                        await sequelize.model('topic').update({ totalMccq: count }, { where: { id: topicId }, fields: ['totalMccq'] })
                    }
                }
                if (_.includes(instance.fields, 'subjectId')) {
                    const mccqData = await sequelize.model('mccq').findOne({ where: { uuidIdentifier: instance.where.uuidIdentifier } })
                    const subjectId = mccqData.dataValues.subjectId
                    const subject = await sequelize.model('subject').findOne({ where: { id: subjectId } })
                    let count1 = subject.dataValues.totalMccq
                    if (count1 && count1 > 0) {
                        count1 = count1 - 1
                        await sequelize.model('subject').update({ totalMccq: count1 }, { where: { id: subjectId }, fields: ['totalMccq'] })
                    }
                }
            },
            afterBulkUpdate: async (instance, options) => {
                ////console.log('after instance-----------------', instance)
                if (_.includes(instance.fields, 'topicId')) {

                    const topic = await sequelize.model('topic').findOne({ where: { id: instance.attributes.topicId } })
                    let count = topic.dataValues.totalMccq
                    ////console.log(count)
                    if (count) count = count + 1
                    else count = 1
                    await sequelize.model('topic').update({ totalMccq: count }, { where: { id: instance.attributes.topicId }, fields: ['totalMccq'] })
                    //////console.log('after instance-----------------', instance)
                    if (_.includes(instance.fields, 'subjectId')) {

                        // update the subject count
                        const subject = await sequelize.model('subject').findOne({ where: { id: instance.attributes.subjectId } })
                        let count1 = subject.dataValues.totalMccq

                        if (count1) count1 = count1 + 1
                        else count1 = 1
                        await sequelize.model('subject').update({ totalMccq: count1 }, { where: { id: instance.attributes.subjectId }, fields: ['totalMccq'] })

                    }
                }
            }
        }
    })


    // mccq relationship
    mccq.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    mccq.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    mccq.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })

    mccq.belongsTo(sequelize.model('subject'),
        {
            foreignKey: {
                allowNull: false
            }
        })

    mccq.belongsTo(sequelize.model('topic'),
        {
            foreignKey: {
                allowNull: false
            }
        })


    const mccq_hashtag = sequelize.define('mccq_hashtag', {
    },
        {
            underscored: true,
            freezeTableName: true,
            hooks: {
                afterCreate: async (instance, options) => {
                    sequelize.model('hashtag').findOne({ where: { id: instance.dataValues.hashtagId } }).then(val => {
                        let noOfMccqs = 1
                        if (val && val.dataValues && val.dataValues.totalMccq) {
                            noOfMccqs = val.dataValues.totalMccq + noOfMccqs
                        }
                        sequelize.model('hashtag').update({ totalMccq: noOfMccqs }, { where: { id: instance.dataValues.hashtagId }, fields: ['totalMccq'] })
                    })
                },
                beforeBulkDestroy: async (instance, options) => {
                    ////console.log(instance, 'before destroy')
                    const mcqHashTagData = await sequelize.model('mccq_hashtag').findAll({ where: { mccqId: instance.where.mccq_id } })
                    if (mcqHashTagData.length > 0) {
                        mcqHashTagData.forEach(async val => {
                            let hashTagId = await sequelize.model('hashtag').findOne({
                                where: {
                                    id: val.hashtagId
                                }
                            })
                            let count1 = hashTagId.dataValues.totalMccq
                            if (count1 && count1 > 0) {
                                count1 = count1 - 1
                                await sequelize.model('hashtag').update({ totalMccq: count1 }, { where: { id: val.hashtagId }, fields: ['totalMccq'] })
                            }
                        })
                    }
                }
            }
        })

    mccq.belongsToMany(sequelize.model('hashtag'),
        {
            as: 'hashtags',
            through: mccq_hashtag
        })

    const mccq_reference = sequelize.define('mccq_reference', {
        description: {
            type: type.TEXT,
            allowNull: true
        }
    }, {
        underscored: true,
        freezeTableName: true,
    })

    mccq.belongsToMany(sequelize.model('reference'), {
        as: 'references',
        through: mccq_reference
    })

    const mccq_key = sequelize.define('mccq_key', {

    }, {
        underscored: true,
        freezeTableName: true,
        hooks: {
            afterCreate: (instance, options) => {
                sequelize.model('key').findOne({ where: { id: instance.dataValues.keyId } }).then(val => {
                    let noOfMccqs = 1
                    if (val && val.dataValues && val.dataValues.totalMccqs) {
                        noOfMccqs = val.dataValues.totalMccqs + noOfMccqs
                    }
                    sequelize.model('key').update({ totalMccqs: noOfMccqs }, { where: { id: instance.dataValues.keyId }, fields: ['totalMccqs'] })
                })
            },
            beforeBulkDestroy: async (instance, options) => {
                ////console.log(instance, 'before destroy', options)
                const mccqKeyData = await sequelize.model('mccq_key').findAll({ where: { mccqId: instance.where.mccq_id } })
                // ////console.log(mccqKeyData)


                if (mccqKeyData.length > 0) {
                    mccqKeyData.forEach(async keyData => {
                        ////console.log(keyData);
                        const key = await sequelize.model('key').findOne({ where: { id: keyData.dataValues.keyId } })
                        let count = key.dataValues.totalMccqs;
                        ////console.log(count);
                        if (count) {
                            count = count - 1
                        }
                        await sequelize.model('key').update({ totalMccqs: count }, { where: { id: keyData.dataValues.keyId }, fields: ['totalMccqs'] })

                    })

                }

            }
        }

    })

    mccq.belongsToMany(sequelize.model('key'), {
        as: 'relatedKeys',
        through: mccq_key
    })
    mccq.belongsTo(sequelize.model('filestore'),
        {
            as: 'questionImageUrl',
            foreignKey: {
                name: 'questionImage',
                allowNull: true
            }
        })
    return mccq
}

/**
 * @swagger
 *  components:
 *    schemas:
 *      MccqWithoutId:
 *        type: object
 *        required:
 *          - question
 *          - subjectId
 *          - topicId
 *          - options
 *          - questionHtml
 *          - explanation
 *          - active
 *          - type
 *        properties:
 *          question:
 *              type: string
 *          subjectId:
 *              type: string
 *              format: uuid
 *          topicId:
 *              type: string
 *              format: uuid
 *          options:
 *              type: array
 *              items:
 *                  type: object
 *                  $ref: '#/components/schemas/MccqOption'
 *          questionHtml:
 *              type: string
 *          explanation:
 *              type: array
 *              items:
 *                  type: object
 *                  $ref: '#/components/schemas/MccqExplanation'
 *          references:
 *            type: array
 *            items:
 *              type: object
 *              $ref: '#/components/schemas/ReferenceForInputForMcq'
 *          hashtags:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          relatedKeys:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          active:
 *            type: boolean
 *          sortNumber:
 *            type: integer
 *          questionImage:
 *            type: string
 *            format: uuid
 *          type:
 *            type: string
 *            enums: [milestoneMccq, tips&tricks, summary, normal]
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      MccqForUpdate:
 *        type: object
 *        properties:
 *          question:
 *              type: string
 *          subjectId:
 *              type: string
 *              format: uuid
 *          topicId:
 *              type: string
 *              format: uuid
 *          options:
 *              type: array
 *              items:
 *                  type: object
 *                  $ref: '#/components/schemas/MccqOption'
 *          questionHtml:
 *              type: string
 *          explanation:
 *              type: array
 *              items:
 *                  type: object
 *                  $ref: '#/components/schemas/MccqExplanation'
 *          active:
 *              type: boolean
 *          sortNumber:
 *              type: integer
 *          questionImage:
 *              type: string
 *              format: uuid
 *          type:
 *            type: string
 *            enums: [milestoneMccq, tips&tricks, summary, normal]
 */




/**
 * @swagger
 *  components:
 *    schemas:
 *      MccqWithId:
 *        type: object
 *        properties:
 *          uuidIdentifier:
 *            type: uuid
 *          question:
 *              type: string
 *          subjectId:
 *              type: string
 *              format: uuid
 *          topicId:
 *              type: string
 *              format: uuid
 *          options:
 *              type: array
 *              items:
 *                  type: object
 *                  $ref: '#/components/schemas/MccqOption'
 *          questionHtml:
 *              type: string
 *          explanation:
 *              type: array
 *              items:
 *                  type: object
 *                  $ref: '#/components/schemas/MccqExplanation'
 *          createdBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          updatedBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          deletedBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          createdAt:
 *              type: string
 *              format: date-time
 *          updatedAt:
 *              type: string
 *              format: date-time
 *          deletedAt:
 *              type: string
 *              format: date-time
 *          active:
 *            type: boolean
 *          sortNumber:
 *            type: integer
 *          references:
 *            type: array
 *            items:
 *              type: object
 *              $ref: '#/components/schemas/ReferenceForInputForMcq'
 *          hashtags:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          relatedKeys:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          questionImageUrl:
 *             type: object
 *             $ref: '#/components/schemas/ImageUrl'
 *          type:
 *            type: string
 *            enums: [milestoneMccq, tips&tricks, summary, normal]
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      MccqOption:
 *        type: object
 *        required:
 *          - id
 *          - option
 *          - optionHtml
 *          - isCorrect
 *        properties:
 *          id:
 *              type: string
 *              format: uuid
 *          option:
 *              type: string
 *          optionHtml:
 *              type: string
 *          isCorrect:
 *              type: boolean
 */

/**
* @swagger
*  components:
*    schemas:
*      MccqExplanation:
*        type: object
*        required:
*          - contentType
*          - contentData
*          - contentDataHtml
*        properties:
*          key:
*              type: integer
*              format: int32
*          contentType:
*              type: string
*          contentData:
*              type: string
*          contentDataHtml:
*              type: string
*/


/**
 * @swagger
 *  components:
 *    schemas:
 *      MccqForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              recover:
 *                  type: boolean
 *              active:
 *                type: boolean
 *              subjectId:
 *                type: string
 *                format: uuid
 *              topicId:
 *                type: string
 *                format: uuid
 */