module.exports = (sequelize, type) => {
    const todayMcq = sequelize.define('todayMcq', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        active: {
            type: type.BOOLEAN,
            allowNull: true,
            defaultValue: true
        },
        date: {
            type: type.DATEONLY,
            allowNull: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        }
    }, {
        underscored: true,
        freezeTableName: false
    });

    todayMcq.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    todayMcq.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    todayMcq.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })

    todayMcq.belongsTo(sequelize.model('mcq'),
        {
            foreignKey: {
                allowNull: true
            }
        })

    todayMcq.belongsTo(sequelize.model('classes'),
        {
            as: 'class',
            foreignKey: {
                allowNull: true
            }
        })

    return todayMcq;
};


/**
 * @swagger
 *  components:
 *    schemas:
 *      TodayMcqWithoutId:
 *        type: object
 *        required:
 *          - date
 *          - mcqId
 *          - active
 *        properties:
 *          date:
 *            type: string
 *            format: date
 *          mcqId:
 *            type: string
 *            format: uuid
 *          active:
 *            type: boolean
 *          classId:
 *            type: string
 *            format: uuid
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      TodayMcqForUpdate:
 *        type: object
 *        properties:
 *          date:
 *            type: string
 *            format: date
 *          mcqId:
 *            type: string
 *            format: uuid
 *          active:
 *            type: boolean
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      todayMcqWithId:
 *        type: object
 *        properties:
 *          uuidIdentifier:
 *            type: uuid
 *          active:
 *            type: boolean
 *          date:
 *            type: string
 *            format: date
 *          mcq:
 *            $ref: '#/components/schemas/McqWithId'
 *          createdBy:
 *            type: string
 *            format: uuid
 *          updatedBy:
 *            type: string
 *            format: uuid
 *          deletedBy:
 *            type: string
 *            format: uuid
 *          createdAt:
 *            type: string
 *            format: date
 *          updatedAt:
 *            type: string
 *            format: date
 *          deletedAt:
 *            type: string
 *            format: date
 */
