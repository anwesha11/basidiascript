module.exports = (sequelize, type) => {
    const score = sequelize.define('score', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        chemistryTotalScore: {
            type: type.INTEGER,
            allowNull: false
        },
        physicsTotalScore: {
            type: type.INTEGER,
            allowNull: false,
        },
        overAllScore: {
            type: type.INTEGER,
            allowNull: false
        },
        chemistryCorrectAnswer: {
            type: type.INTEGER,
            allowNull: false
        },
        chemistryInCorrectAnswer: {
            type: type.INTEGER,
            allowNull: false,
        },
        chemistryUnattemptedAnswer: {
            type: type.INTEGER,
            allowNull: false
        },
        physicsCorrectAnswer: {
            type: type.INTEGER,
            allowNull: false
        },
        physicsInCorrectAnswer: {
            type: type.INTEGER,
            allowNull: false,
        },
        physicsUnattemptedAnswer: {
            type: type.INTEGER,
            allowNull: false
        },
       
        overAllCorrectAnswer: {
            type: type.INTEGER,
            allowNull: false
        },
        overAllInCorrectAnswer: {
            type: type.INTEGER,
            allowNull: false,
        },
        overAllUnattemptedAnswer: {
            type: type.INTEGER,
            allowNull: false
        },
        overallActualScore: {
            type: type.INTEGER,
            allowNull: true
        },
        physicsActualScore: {
            type: type.INTEGER,
            allowNull: true
        },
        chemistryActualScore: {
            type: type.INTEGER,
            allowNull: true
        },
        timeTaken:{
            type: type.INTEGER,
            allowNull: false
        },
        testStartTime:{
            type: type.BIGINT,
            allowNull:false
        },
        zoologyCorrectAnswer: {
            type: type.INTEGER,
            allowNull: true
        },
        zoologyInCorrectAnswer: {
            type: type.INTEGER,
            allowNull: true,
        },
        zoologyUnattemptedAnswer: {
            type: type.INTEGER,
            allowNull: true
        },
        zoologyActualScore:{
            type: type.INTEGER,
            allowNull: true
        },
        botanyCorrectAnswer: {
            type: type.INTEGER,
            allowNull: true
        },
        botanyInCorrectAnswer: {
            type: type.INTEGER,
            allowNull: true,
        },
        botanyUnattemptedAnswer: {
            type: type.INTEGER,
            allowNull: true
        },
        botanyActualScore:{
            type: type.INTEGER,
            allowNull: true
        },
        botanyTotalScore:{
            type: type.INTEGER,
            allowNull: true
        },
        zoologyTotalScore:{
            type: type.INTEGER,
            allowNull: true
        }
    }, {
        underscored: true,
        freezeTableName: true
    })

    score.belongsTo(sequelize.model('test'),{
        foreignKey: {
            allowNull: false
        }
    })

    score.belongsTo(sequelize.model('user'),{
        foreignKey: {
            allowNull: false
        }
    })
    return score
    
}

