module.exports = (sequelize, type) => {
    const app = sequelize.define('app', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        name: {
            type: type.STRING,
            allowNull: false,
            unique: {
                msg: 'name is already in use'
            }
        },
        description: {
            type: type.TEXT,
            allowNull: false
        }
    }, {
        underscored: true,
        freezeTableName: true
    })
    return app
}

/**
 * @swagger
 *  components:
 *    schemas:
 *      AppWithoutId:
 *        type: object
 *        required:
 *          - name
 *          - description
 *        properties:
 *          name:
 *            type: string
 *          description:
 *            type: string
 */
