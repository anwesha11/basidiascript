const _ = require('lodash')
module.exports = (sequelize, type) => {
    const fillInTheBlank = sequelize.define('fillInTheBlank', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        question: {
            type: type.TEXT,
            allowNull: false
        },
        optionArray: {
            type: type.JSONB,
            allowNull: false,
        },
        active: {
            type: type.BOOLEAN,
            allowNull: true,
            defaultValue: true
        },
        sortNumber: {
            type: type.INTEGER,
            allowNull: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        isMilestoneFillInTheBlank: {
            type: type.BOOLEAN,
            allowNull: false
        },
        explanation: {
            type: type.JSONB,
            allowNull: true,
        }
    }, {
        underscored: true,
        hooks: {
            afterCreate: (instance, options) => {
                ////console.log('instance-----------------', instance)
                sequelize.model('topic').findOne({ where: { id: instance.dataValues.topicId } }).then(val => {
                    ////console.log('topicVal', val)
                    let totalFitb = 1
                    if (val.dataValues.totalFitb) totalFitb = val.dataValues.totalFitb + totalFitb
                    sequelize.model('topic').update({ totalFitb }, { where: { id: instance.dataValues.topicId }, fields: ['totalFitb'] })
                })
                sequelize.model('subject').findOne({ where: { id: instance.dataValues.subjectId } }).then(val => {
                    let noOfFitbs = 1
                    if (val && val.dataValues && val.dataValues.totalFitb) {
                        noOfFitbs = val.dataValues.totalFitb + noOfFitbs
                    }
                    sequelize.model('subject').update({ totalFitb: noOfFitbs }, { where: { id: instance.dataValues.subjectId }, fields: ['totalFitb'] })
                })
            },
            beforeBulkUpdate: async (instance, options) => {
                ////console.log('Beforfe instance-----------------', instance)

                if (_.includes(instance.fields, 'topicId')) {
                    const fillInTheBlankData = await sequelize.model('fillInTheBlank').findOne({ where: { uuidIdentifier: instance.where.uuidIdentifier } })

                    const topicId = fillInTheBlankData.dataValues.topicId

                    // update the fillInTheBlank count
                    const topic = await sequelize.model('topic').findOne({ where: { id: topicId } })
                    let count = topic.dataValues.totalFitb
                    if (count && count > 0) {
                        count = count - 1
                        await sequelize.model('topic').update({ totalFitb: count }, { where: { id: topicId }, fields: ['totalFitb'] })

                    }
                }
                if (_.includes(instance.fields, 'subjectId')) {
                    const fillData = await sequelize.model('fillInTheBlank').findOne({ where: { uuidIdentifier: instance.where.uuidIdentifier } })
                    const subjectId = fillData.dataValues.subjectId
                    const subject = await sequelize.model('subject').findOne({ where: { id: subjectId } })
                    let count1 = subject.dataValues.totalFitb
                    if (count1 && count1 > 0) {
                        count1 = count1 - 1
                        await sequelize.model('subject').update({ totalFitb: count1 }, { where: { id: subjectId }, fields: ['totalFitb'] })
                    }
                }
            },
            afterBulkUpdate: async (instance, options) => {
                ////console.log('after instance-----------------', instance)
                if (_.includes(instance.fields, 'topicId')) {

                    const topic = await sequelize.model('topic').findOne({ where: { id: instance.attributes.topicId } })
                    let count = topic.dataValues.totalFitb
                    ////console.log(count)
                    if (count) count = count + 1
                    else count = 1
                    await sequelize.model('topic').update({ totalFitb: count }, { where: { id: instance.attributes.topicId }, fields: ['totalFitb'] })
                }
                if (_.includes(instance.fields, 'subjectId')) {

                    // update the subject count
                    const subject = await sequelize.model('subject').findOne({ where: { id: instance.attributes.subjectId } })
                    let count1 = subject.dataValues.totalFitb

                    if (count1) count1 = count1 + 1
                    else count1 = 1
                    await sequelize.model('subject').update({ totalFitb: count1 }, { where: { id: instance.attributes.subjectId }, fields: ['totalFitb'] })

                }
            }
        }


    })


    // fillInTheBlank relationship
    fillInTheBlank.belongsTo(sequelize.model('user'), {
        as: 'creationUser',
        foreignKey: {
            name: 'createdBy',
            allowNull: false,
        }
    })

    fillInTheBlank.belongsTo(sequelize.model('user'), {
        as: 'updationUser',
        foreignKey: {
            name: 'updatedBy',
            allowNull: false
        }
    })

    fillInTheBlank.belongsTo(sequelize.model('user'), {
        as: 'deletionUser',
        foreignKey: {
            name: 'deletedBy',
            allowNull: true
        }
    })
    fillInTheBlank.belongsTo(sequelize.model('subject'), {
        foreignKey: {
            allowNull: false
        }
    })

    const fillup_hashtag = sequelize.define('fillup_hashtag', {

    },
        {
            underscored: true,
            freezeTableName: true,
            hooks: {
                afterCreate: async (instance, options) => {

                    sequelize.model('hashtag').findOne({ where: { id: instance.dataValues.hashtagId } }).then(val => {
                        let noOffills = 1
                        if (val && val.dataValues && val.dataValues.totalFitb) {
                            noOffills = val.dataValues.totalFitb + noOffills
                        }
                        sequelize.model('hashtag').update({ totalFitb: noOffills }, { where: { id: instance.dataValues.hashtagId }, fields: ['totalFitb'] })
                    })
                }
            }
        })



    fillInTheBlank.belongsToMany(sequelize.model('hashtag'),
        {
            as: 'hashtags',
            through: fillup_hashtag
        })

    fillInTheBlank.belongsTo(sequelize.model('topic'), {
        foreignKey: {
            allowNull: false
        }
    })
    const fillup_reference = sequelize.define('fillup_reference', {
        description: {
            type: type.TEXT,
            allowNull: true
        },
        sortNumber: {
            type: type.INTEGER,
            allowNull: true
        }
    }, {
        underscored: true,
        freezeTableName: true,
    })
    fillInTheBlank.belongsToMany(sequelize.model('reference'), {
        as: 'references',
        through: fillup_reference
    })

    return fillInTheBlank
}

/**
 * @swagger
 *  components:
 *    schemas:
 *      FillInTheBlankQuestionWithoutId:
 *        type: object
 *        required:
 *          - question
 *          - optionArray
 *          - active
 *          - subjectId
 *          - topicId
 *          - isMilestoneFillInTheBlank
 *        properties:
 *          question:
 *              type: string
 *          optionArray:
 *              type: array
 *              items:
 *                  type: object
 *                  $ref: '#/components/schemas/FillInTheBlankQuestionOption'
 *          active:
 *              type: boolean
 *          sortnumber:
 *              type: number
 *              format: int32
 *          subjectId:
 *              type: string
 *              format: uuid
 *          topicId:
 *              type: string
 *              format: uuid
 *          isMilestoneFillInTheBlank:
 *              type: boolean
 *          references:
 *            type: array
 *            items:
 *              type: object
 *              $ref: '#/components/schemas/ReferenceForInput'
 *          explanation:
 *              type: array
 *              items:
 *                  type: object
 *                  $ref: '#/components/schemas/MccqExplanation'
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      FillInTheBlankQuestionForUpdate:
 *        type: object
 *        properties:
 *          question:
 *              type: string
 *          optionArray:
 *              type: array
 *              items:
 *                  type: object
 *                  $ref: '#/components/schemas/FillInTheBlankQuestionOption'
 *          active:
 *            type: boolean
 *          subjectId:
 *              type: string
 *              format: uuid
 *          topicId:
 *              type: string
 *              format: uuid
 *          isMilestoneFillInTheBlank:
 *              type: boolean
 *          references:
 *            type: array
 *            items:
 *              type: object
 *              $ref: '#/components/schemas/ReferenceForInput'
 *          explanation:
 *              type: array
 *              items:
 *                  type: object
 *                  $ref: '#/components/schemas/MccqExplanation'
 *
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      FillInTheBlankQuestionForBulkUpdate:
 *        type: object
 *        properties:
 *          recover:
 *            type: boolean
 *          active:
 *            type: boolean
 *          subjectId:
 *              type: string
 *              format: uuid
 *          topicId:
 *              type: string
 *              format: uuid
 *
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      FillInTheBlankQuestionWithId:
 *        type: object
 *        properties:
 *          uuidIdentifier:
 *            type: uuid
 *          question:
 *              type: string
 *          optionArray:
 *              type: array
 *              items:
 *                  type: object
 *                  $ref: '#/components/schemas/FillInTheBlankQuestionOption'
 *          createdBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          updatedBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          deletedBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          createdAt:
 *              type: string
 *              format: date-time
 *          updatedAt:
 *              type: string
 *              format: date-time
 *          deletedAt:
 *              type: string
 *              format: date-time
 *          active:
 *            type: boolean
 *          subjectId:
 *              type: string
 *              format: uuid
 *          topicId:
 *              type: string
 *              format: uuid
 *          isMilestoneFillInTheBlank:
 *              type: boolean
 *          references:
 *            type: array
 *            items:
 *              type: object
 *              $ref: '#/components/schemas/ReferenceForInput'
 *          explanation:
 *              type: array
 *              items:
 *                  type: object
 *                  $ref: '#/components/schemas/MccqExplanation'
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      FillInTheBlankQuestionOption:
 *        type: object
 *        required:
 *          - options
 *          - correctOption
 *          - optionNumber
 *        properties:
 *          options:
 *              type: array
 *              items:
 *                  type: string
 *          correctOption:
 *              type: string
 *          optionNumber:
 *              type: number
 *              format: int32
 */