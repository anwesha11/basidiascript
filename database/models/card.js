module.exports = (sequelize, type) => {
    const card = sequelize.define('card', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        title: {
            type: type.STRING,
            allowNull: false
        },
        url: {
            type: type.STRING,
            allowNull: false
        },
        active: {
            type: type.BOOLEAN,
            allowNull: false,
            defaultValue: true
        },
        sortNumber: {
            type: type.INTEGER,
            allowNull: true
        },
        showFor: {
            type: type.INTEGER,
            allowNull: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
    }, {
        underscored: true,
        freezeTableName: true,
    });

    card.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    card.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    card.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })
    card.belongsTo(sequelize.model('filestore'),
        {
            as: 'thumbnailUrl',
            foreignKey: {
                name: 'thumbnail',
                allowNull: true
            }
        })
    card.belongsTo(sequelize.model('classes'), {
        as: 'class',
        foreignKey: {
            name: 'classId',
            allowNull: true
        }
    })

    return card;
};

/**
 * @swagger
 *  components:
 *    schemas:
 *      cardWithId:
 *        type: object
 *        required:
 *          - title
 *          - url
 *          - classId
 *          - thumbnail
 *          - active
 *          - sortNumber
 *        properties:
 *          title:
 *            type: string
 *          classId:
 *            type: string
 *            format: uuid
 *          url:
 *            type: string
 *            format: uri
 *          thumbnail:
 *            type: string
 *            format: uuid
 *          active:
 *            type: boolean
 *          sortNumber:
 *            type: integer
 *            format: int32
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      cardForUpdate:
 *        type: object
 *        properties:
 *          title:
 *            type: string
 *          classId:
 *            type: string
 *            format: uuid
 *          url:
 *            type: string
 *            format: uri
 *          thumbnail:
 *            type: string
 *            format: uuid
 *          active:
 *            type: boolean
 *          sortNumber:
 *            type: integer
 *            format: int32
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      cardWithoutId:
 *        type: object
 *        properties:
 *          title:
 *            type: string
 *          classId:
 *            type: string
 *            format: uuid
 *          url:
 *            type: string
 *            format: uri
 *          thumbnail:
 *            type: string
 *            format: uuid
 *          active:
 *            type: boolean
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          createdBy:
 *            type: object
 *            $ref: '#/components/schemas/CreatedBy'
 *          updatedBy:
 *            type: object
 *            $ref: '#/components/schemas/CreatedBy'
 *          deletedBy:
 *            type: object
 *            $ref: '#/components/schemas/CreatedBy'
 *          createdAt:
 *            type: string
 *            format: date
 *          updatedAt:
 *            type: string
 *            format: date
 *          deletedAt:
 *            type: string
 *            format: date
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      cardBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              recover:
 *                type: boolean
 *              active:
 *                type: boolean
 */