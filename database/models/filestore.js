module.exports = (sequelize, type) => {
    return sequelize.define('filestore', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        url: {
            type: type.TEXT,
            allowNull: false
        },
        isUsed: {
            type: type.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        underscored: true,
        freezeTableName: true,
    })
}
/**
 * @swagger
 *  components:
 *    schemas:
 *      ImageUrl:
 *        type: object
 *        required:
 *          - url
 *        properties:
 *          url:
 *            type: string
 *            format: uri
 */