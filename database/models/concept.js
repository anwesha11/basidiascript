module.exports = (sequelize, type) => {
    const concept = sequelize.define('concept', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        name: {
            type: type.STRING,
            allownull: false
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        startDate: {
            type: type.DATEONLY,
            allowNull: true
        },
        endDate: {
            type: type.DATEONLY,
            allowNull: true
        },
        carousels: {
            type: type.JSONB,
            allowNull: true
        },
        active: {
            type: type.BOOLEAN,
            defaultValue: true
        },
        sortNumber: {
            type: type.INTEGER,
            allowNull: true,
            defaultValue: 0
        }
    }, {
        underscored: true,
        freezeTableName: true,
    })

    concept.belongsTo(sequelize.model('filestore'),
        {
            as: 'thumbnailUrl',
            foreignKey: {
                name: 'thumbnail',
                allowNull: true
            }
        })

    concept.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    concept.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    concept.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })

    concept.belongsTo(sequelize.model('classes'),
        {
            foreignKey: {
                allowNull: false
            }
        })

    concept.belongsTo(sequelize.model('topic'),
        {
            foreignKey: {
                allowNull: false
            }
        })
    sequelize.model('topic').hasMany(concept)
}


/**
 * @swagger
 *  components:
 *    schemas:
 *      ConceptWithoutId:
 *        type: object
 *        required:
 *          - name
 *          - classId
 *          - topicId
 *          - startDate
 *          - endDate
 *          - active
 *        properties:
 *          name:
 *            type: string
 *          classId:
 *            type: string
 *            format: uuid
 *          topicId:
 *            type: string
 *            format: uuid
 *          thumbnail:
 *            type: string
 *            format: uuid
 *          startDate:
 *            type: string
 *            format: date
 *          endDate:
 *            type: string
 *            format: date
 *          carousels:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          active:
 *              type: boolean
 *          sortNumber:
 *              type: integer
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      ConceptForUpdate:
 *        type: object
 *        properties:
 *          name:
 *            type: string
 *          classId:
 *            type: string
 *            format: uuid
 *          topicId:
 *            type: string
 *            format: uuid
 *          thumbnail:
 *            type: string
 *            format: uuid
 *          startDate:
 *            type: string
 *            format: date
 *          endDate:
 *            type: string
 *            format: date
 *          carousels:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          active:
 *              type: boolean
 *          sortNumber:
 *              type: integer
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      ConceptForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              active:
 *                  type: boolean
 */

/**
* @swagger
*  components:
*    schemas:
*      ConceptWithId:
*        type: object
*        properties:
*          uuidIdentifier:
*            type: uuid
*          name:
*            type: string
*          startDate:
*            type: string
*            format: date
*          endDate:
*            type: string
*            format: date
*          classId:
*            type: string
*            format: uuid
*          topicId:
*            type: string
*            format: uuid
*          carousels:
*            type: array
*            items:
*              type: string
*              format: uuid
*          active:
*              type: boolean
*          sortNumber:
*              type: integer
*          createdBy:
*            type: object
*            $ref: '#/components/schemas/CreatedBy'
*          updatedBy:
*            type: object
*            $ref: '#/components/schemas/CreatedBy'
*          deletedBy:
*            type: object
*            $ref: '#/components/schemas/CreatedBy'
*          thumbnail:
*            type: object
*            $ref: '#/components/schemas/ImageUrl'
*          createdAt:
*            type: string
*            format: date
*          updatedAt:
*            type: string
*            format: date
*          deletedAt:
*            type: string
*            format: date
*/