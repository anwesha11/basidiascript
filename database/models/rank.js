module.exports = (sequelize, type) => {
    const rank = sequelize.define('rank', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        rank: {
            type: type.STRING,
            allowNull: false
        }, 
        percentile: {
            type: type.INTEGER,
            allowNull: true
        },
        mode: {
            type: type.INTEGER,
            allowNull: true
        }
    }, {
        freezeTableName: true,
        underscored: true
    });
    rank.belongsTo(sequelize.model('test'),{
        foreignKey: {
            allowNull: false
        }
    })

    rank.belongsTo(sequelize.model('user'),{
        foreignKey: {
            allowNull: false
        }
    })
    return rank;
};