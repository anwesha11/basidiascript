const _ = require('lodash')
const Sequelize = require('sequelize')
const Op = Sequelize.Op
module.exports = (sequelize, type) => {
    const mcq = sequelize.define('mcq', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        question: {
            type: type.TEXT,
            allowNull: false
        },
        option1: {
            type: type.TEXT,
            allowNull: false,
        },
        option2: {
            type: type.TEXT,
            allowNull: false,
        },
        option3: {
            type: type.TEXT,
            allowNull: false,
        },
        option4: {
            type: type.TEXT,
            allowNull: false,
        },
        questionHtml: {
            type: type.TEXT,
            allowNull: false
        },
        option1Html: {
            type: type.TEXT,
            allowNull: false,
        },
        option2Html: {
            type: type.TEXT,
            allowNull: false,
        },
        option3Html: {
            type: type.TEXT,
            allowNull: false,
        },
        option4Html: {
            type: type.TEXT,
            allowNull: false,
        },
        explanation: {
            type: type.JSONB,
            allowNull: true,
        },
        correctOption: {
            type: type.INTEGER,
            allowNull: true,
            validate: {
                min: 0,
                max: 3
            }
        },
        totalNoOfAttempt: {
            type: type.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        totalCorrectAttempt: {
            type: type.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        sortNumber: {
            type: type.INTEGER,
            allowNull: true
        },
        questionWithoutKatex: {
            type: type.TEXT,
            allowNull: false,
        },
        active: {
            type: type.BOOLEAN,
            allowNull: true,
            defaultValue: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        isMilestoneMcq: {
            type: type.BOOLEAN,
            allowNull: false
        }
    }, {
        underscored: true,
        freezeTableName: true,
        hooks: {
            afterCreate: (instance, options) => {
                ////console.log('instance-----------------', instance)
                sequelize.model('topic').findOne({ where: { id: instance.dataValues.topicId } }).then(val => {
                    ////console.log('topicVal', val)
                    let totalMcq = 1
                    if (val.dataValues.totalMcq) totalMcq = val.dataValues.totalMcq + totalMcq
                    sequelize.model('topic').update({ totalMcq }, { where: { id: instance.dataValues.topicId }, fields: ['totalMcq'] })
                })
                sequelize.model('subject').findOne({ where: { id: instance.dataValues.subjectId } }).then(val => {
                    let noOfMcq = 1
                    if (val && val.dataValues && val.dataValues.totalMcq) {
                        noOfMcq = val.dataValues.totalMcq + noOfMcq
                    }
                    sequelize.model('subject').update({ totalMcq: noOfMcq }, { where: { id: instance.dataValues.subjectId }, fields: ['totalMcq'] })
                })
            },
            beforeBulkUpdate: async (instance, options) => {
                ////console.log('Beforfe instance-----------------', instance)
                if (_.includes(instance.fields, 'subjectId')) {
                    const mcqData = await sequelize.model('mcq').findOne({ where: { uuidIdentifier: instance.where.uuidIdentifier } })
                    const subjectId = mcqData.dataValues.subjectId
                    const subject = await sequelize.model('subject').findOne({ where: { id: subjectId } })
                    let count1 = subject.dataValues.totalMcq
                    if (count1 && count1 > 0) {
                        count1 = count1 - 1
                        await sequelize.model('subject').update({ totalMcq: count1 }, { where: { id: subjectId }, fields: ['totalMcq'] })
                    }
                }
                if (_.includes(instance.fields, 'topicId')) {
                    const mcqData = await sequelize.model('video').findOne({ where: { uuidIdentifier: instance.where.uuidIdentifier } })

                    const topicId = mcqData.dataValues.topicId

                    // update the mcq count
                    const topic = await sequelize.model('topic').findOne({ where: { id: topicId } })
                    let count = topic.dataValues.totalMcq
                    if (count && count > 0) {
                        count = count - 1
                        await sequelize.model('topic').update({ totalMcq: count }, { where: { id: topicId }, fields: ['totalMcq'] })
                    }
                }
            },
            afterBulkUpdate: async (instance, options) => {
                ////console.log('after instance-----------------', instance)
                if (_.includes(instance.fields, 'topicId')) {

                    const topic = await sequelize.model('topic').findOne({ where: { id: instance.attributes.topicId } })
                    let count = topic.dataValues.totalMcq
                    ////console.log(count)
                    if (count) count = count + 1
                    else count = 1
                    await sequelize.model('topic').update({ totalMcq: count }, { where: { id: instance.attributes.topicId }, fields: ['totalMcq'] })
                }
                if (_.includes(instance.fields, 'subjectId')) {

                    // update the subject count
                    const subject = await sequelize.model('subject').findOne({ where: { id: instance.attributes.subjectId } })
                    let count1 = subject.dataValues.totalMcq

                    if (count1) count1 = count1 + 1
                    else count1 = 1
                    await sequelize.model('subject').update({ totalMcq: count1 }, { where: { id: instance.attributes.subjectId }, fields: ['totalMcq'] })

                }
            }
        }

    })

    // mcq relationship
    mcq.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    mcq.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    mcq.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })

    mcq.belongsTo(sequelize.model('subject'),
        {
            foreignKey: {
                allowNull: false
            }
        })

    mcq.belongsTo(sequelize.model('topic'),
        {
            foreignKey: {
                allowNull: false
            }
        })
    sequelize.model('topic').hasMany(mcq)

    mcq.belongsTo(sequelize.model('filestore'),
        {
            as: 'questionImageUrl',
            foreignKey: {
                name: 'questionImage',
                allowNull: true
            }
        })


    const mcq_hashtag = sequelize.define('mcq_hashtag', {},
        {
            underscored: true,
            freezeTableName: true,
            hooks: {
                afterCreate: async (instance, options) => {
                    sequelize.model('hashtag').findOne({ where: { id: instance.dataValues.hashtagId } }).then(val => {
                        let noOfMcqs = 1
                        if (val && val.dataValues && val.dataValues.totalMcq) {
                            noOfMcqs = val.dataValues.totalMcq + noOfMcqs
                        }
                        sequelize.model('hashtag').update({ totalMcq: noOfMcqs }, { where: { id: instance.dataValues.hashtagId }, fields: ['totalMcq'] })
                    })
                },
                beforeBulkDestroy: async (instance, options) => {
                    ////console.log(instance, 'before destroy')
                    let mcqHashTagData = await sequelize.model('mcq_hashtag').findAll({ where: { mcqId: instance.where.mcq_id } })
                    if (mcqHashTagData.length > 0) {
                        mcqHashTagData.forEach(async val => {
                            let hashTagId = await sequelize.model('hashtag').findOne({
                                where: {
                                    id: val.hashtagId
                                }
                            })
                            let count1 = hashTagId.dataValues.totalMcq
                            if (count1 && count1 > 0) {
                                count1 = count1 - 1
                                await sequelize.model('hashtag').update({ totalMcq: count1 }, { where: { id: val.hashtagId }, fields: ['totalMcq'] })
                            }

                        })
                    }
                }
            }
        })

    mcq.belongsToMany(sequelize.model('hashtag'),
        {
            as: 'hashtags',
            through: mcq_hashtag
        })

    const mcq_reference = sequelize.define('mcq_reference', {
        description: {
            type: type.TEXT,
            allowNull: true
        }
    }, {
        underscored: true,
        freezeTableName: true
    })
    mcq.belongsToMany(sequelize.model('reference'), {
        as: 'references',
        through: mcq_reference
    })

    const mcq_key = sequelize.define('mcq_key', {}, {
        underscored: true,
        freezeTableName: true,
        hooks: {
            afterCreate: (instance, options) => {
                sequelize.model('key').findOne({ where: { id: instance.dataValues.keyId } }).then(val => {
                    let noOfMcqs = 1
                    if (val && val.dataValues && val.dataValues.totalMcqs) {
                        noOfMcqs = val.dataValues.totalMcqs + noOfMcqs
                    }
                    sequelize.model('key').update({ totalMcqs: noOfMcqs }, { where: { id: instance.dataValues.keyId }, fields: ['totalMcqs'] })
                })
            },
            beforeBulkDestroy: async (instance, options) => {
                ////console.log(instance, 'before destroy', options)
                const mcqKeyData = await sequelize.model('mcq_key').findAll({ where: { mcqId: instance.where.mcq_id } })
                if (mcqKeyData.length > 0) {
                    mcqKeyData.forEach(async keyData => {
                        ////console.log(keyData);
                        const key = await sequelize.model('key').findOne({ where: { id: keyData.dataValues.keyId } })
                        let count = key.dataValues.totalMcqs;
                        ////console.log(count);
                        if (count) {
                            count = count - 1
                        }
                        await sequelize.model('key').update({ totalMcqs: count }, { where: { id: keyData.dataValues.keyId }, fields: ['totalMcqs'] })

                    })

                }
            }
        }
    })


    mcq.belongsToMany(sequelize.model('key'), {
        as: 'relatedKeys',
        through: mcq_key
    })

    sequelize.model('key').belongsToMany(mcq, {
        as: 'relatedMcqs',
        through: mcq_key,
        allowNull: true
    })
    const mcq_test = sequelize.define('mcq_test', {
        sortNumber: {
            type: type.INTEGER,
            allowNull: true
        },
        section: {
            type: type.INTEGER,
            allowNull: true,
            validate: {
                min: 0,
                max: 1
            }
        }
    }, {
        underscored: true,
        freezeTableName: true
    })
    mcq.belongsToMany(sequelize.model('test'), {
        as: 'test',
        through: mcq_test,
        allowNull: true
    })
    sequelize.model('test').belongsToMany(mcq,
        {
            as: 'mcqs',
            through: mcq_test,
            allowNull: true
        })
    mcq.hasMany(sequelize.model('mcq_test'))
    sequelize.model('test').hasMany(sequelize.model('mcq_test'))
    return mcq
}

/**
 * @swagger
 *  components:
 *    schemas:
 *      McqWithoutId:
 *        type: object
 *        required:
 *          - question
 *          - subjectId
 *          - topicId
 *          - option1
 *          - option2
 *          - option3
 *          - option4
 *          - questionWithoutKatex
 *          - questionHtml
 *          - option1Html
 *          - option2Html
 *          - option3Html
 *          - option4Html
 *          - explanation
 *          - correctOption
 *          - isMilestoneMcq
 *        properties:
 *          question:
 *              type: string
 *          subjectId:
 *              type: string
 *              format: uuid
 *          topicId:
 *              type: string
 *              format: uuid
 *          testId:
 *              type: string
 *              format: uuid
 *          questionImage:
 *              type: string
 *              format: uuid
 *          questionWithoutKatex:
 *              type: string
 *          option1:
 *              type: string
 *          option2:
 *              type: string
 *          option3:
 *              type: string
 *          option4:
 *              type: string
 *          questionHtml:
 *              type: string
 *          option1Html:
 *              type: string
 *          option2Html:
 *              type: string
 *          option3Html:
 *              type: string
 *          option4Html:
 *              type: string
 *          explanation:
 *              type: array
 *              items:
 *                  type: object
 *                  $ref: '#/components/schemas/MccqExplanation'
 *          correctOption:
 *              type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          references:
 *            type: array
 *            items:
 *              type: object
 *              $ref: '#/components/schemas/ReferenceForInputForMcq'
 *          hashtags:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          active:
 *            type: boolean
 *          relatedKeys:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          isMilestoneMcq:
 *            type: boolean
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      McqForUpdate:
 *        type: object
 *        properties:
 *          question:
 *              type: string
 *          subjectId:
 *              type: string
 *              format: uuid
 *          topicId:
 *              type: string
 *              format: uuid
 *          questionImage:
 *              type: string
 *              format: uuid
 *          questionWithoutKatex:
 *              type: string
 *          option1:
 *              type: string
 *          option2:
 *              type: string
 *          option3:
 *              type: string
 *          option4:
 *              type: string
 *          questionHtml:
 *              type: string
 *          option1Html:
 *              type: string
 *          option2Html:
 *              type: string
 *          option3Html:
 *              type: string
 *          option4Html:
 *              type: string
 *          explanation:
 *              type: array
 *              items:
 *                  type: object
 *                  $ref: '#/components/schemas/MccqExplanation'
 *          correctOption:
 *              type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          references:
 *            type: array
 *            items:
 *              type: object
 *              $ref: '#/components/schemas/ReferenceForInputForMcq'
 *          hashtags:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          active:
 *            type: boolean
 *          relatedKeys:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          isMilestoneMcq:
 *            type: boolean
 */




/**
 * @swagger
 *  components:
 *    schemas:
 *      McqWithId:
 *        type: object
 *        properties:
 *          uuidIdentifier:
 *            type: uuid
 *          question:
 *              type: string
 *          subjectId:
 *              type: string
 *              format: uuid
 *          topicId:
 *              type: string
 *              format: uuid
 *          questionImage:
 *              type: string
 *          option1:
 *              type: string
 *          option2:
 *              type: string
 *          option3:
 *              type: string
 *          option4:
 *              type: string
 *          questionHtml:
 *              type: string
 *          option1Html:
 *              type: string
 *          option2Html:
 *              type: string
 *          option3Html:
 *              type: string
 *          option4Html:
 *              type: string
 *          explanation:
 *              type: array
 *              items:
 *                  type: object
 *                  $ref: '#/components/schemas/MccqExplanation'
 *          correctOption:
 *              type: string
 *          questionWithoutKatex:
 *              type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          createdBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          updatedBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          deletedBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          createdAt:
 *              type: string
 *              format: date-time
 *          updatedAt:
 *              type: string
 *              format: date-time
 *          deletedAt:
 *              type: string
 *              format: date-time
 *          references:
 *            type: array
 *            items:
 *              type: object
 *              $ref: '#/components/schemas/ReferenceForInputForMcq'
 *          hashtags:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          relatedKeys:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          questionImageUrl:
 *             type: object
 *             $ref: '#/components/schemas/ImageUrl'
 *          isMilestoneMcq:
 *             type: boolean
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      McqForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              recover:
 *                type: boolean
 *              active:
 *                type: boolean
 *              subjectId:
 *                type: string
 *                format: uuid
 *              topicId:
 *                type: string
 *                format: uuid
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      UntagTestMcq:
 *        type: object
 *        required:
 *          - mcqId
 *          - testId
 *        properties:
 *          mcqId:
 *            type: string
 *            format: uuid
 *          testId:
 *            type: string
 *            format: uuid
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      AddMultipleTestMcq:
 *        type: object
 *        required:
 *          - mcqIds
 *          - testId
 *        properties:
 *          mcqIds:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          testId:
 *            type: string
 *            format: uuid
 */