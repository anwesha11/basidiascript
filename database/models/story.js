const _ = require('lodash')
module.exports = (sequelize, type) => {
    const story = sequelize.define('story', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        title: {
            type: type.TEXT,
            allowNull: false
        },
        active: {
            type: type.BOOLEAN,
            allowNull: true,
            defaultValue: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        sortNumber: {
            type: type.INTEGER,
            allowNull: true
        },
        status:{
            type: type.INTEGER,
            allowNull: false
        }
    }, {
        underscored: true,
        freezeTableName: true,
        hooks: {
            afterCreate: (instance, options) => {
                sequelize.model('topic').findOne({ where: { id: instance.dataValues.topicId } }).then(val => {
                    ////console.log('topicVal', val)
                    let totalStory = 1
                    if (val.dataValues.totalStory) totalStory = val.dataValues.totalStory + totalStory
                    sequelize.model('topic').update({ totalStory }, { where: { id: instance.dataValues.topicId }, fields: ['totalStory'] })
                })
            },
            afterBulkUpdate: async (instance, options) => {
                if (_.includes(instance.fields, 'topicId')) {
                    const topic = await sequelize.model('topic').findOne({ where: { id: instance.attributes.topicId } })
                    let count = topic.dataValues.totalStory
                    if (count) count = count + 1
                    else count = 1
                    await sequelize.model('topic').update({ totalStory: count }, { where: { id: instance.attributes.topicId }, fields: ['totalStory'] })
                }
                if (_.includes(instance.fields, 'subjectId')) {

                    // update the subject count
                    const subject = await sequelize.model('subject').findOne({ where: { id: instance.attributes.subjectId } })
                    let count1 = subject.dataValues.totalStory
                    let count2 = subject.dataValues.totalStories
                    if (count1) {
                        count1 = count1 + 1
                        count2 = count2 + 1
                    }
                    else {
                        count1 = 1
                        count2 = 1
                    }
                    await sequelize.model('subject').update({ totalStory: count1, totalStories: count2 }, { where: { id: instance.attributes.subjectId }, fields: ['totalStory', 'totalStories'] })

                }
            },
            beforeBulkUpdate: async (instance, options) => {
                if (_.includes(instance.fields, 'subjectId')) {
                    const storyData = await sequelize.model('story').findOne({ where: { uuidIdentifier: instance.where.uuidIdentifier } })
                    const subjectId = storyData.dataValues.subjectId
                    const subject = await sequelize.model('subject').findOne({ where: { id: subjectId } })
                    let count1 = subject.dataValues.totalStory
                    let count2 = subject.dataValues.totalStories
                    if (count1 && count1 > 0) {
                        count1 = count1 - 1
                        await sequelize.model('subject').update({ totalStory: count1 }, { where: { id: subjectId }, fields: ['totalStory'] })
                    }
                    if (count2 && count2 > 0) {
                        count2 = count2 - 1
                        await sequelize.model('subject').update({ totalStories: count2 }, { where: { id: subjectId }, fields: ['totalStories'] })
                    }
                }
                if (_.includes(instance.fields, 'topicId')) {
                    const storyData = await sequelize.model('story').findOne({ where: { uuidIdentifier: instance.where.uuidIdentifier } })
                    const topicId = storyData.dataValues.topicId
                    // update the story count
                    const topic = await sequelize.model('topic').findOne({ where: { id: topicId } })
                    let count = topic.dataValues.totalStory
                    if (count && count > 0) {
                        count = count - 1
                        await sequelize.model('topic').update({ totalStory: count }, { where: { id: topicId }, fields: ['totalStory'] })
                        sequelize.model('subject').findOne({ where: { id: instance.dataValues.subjectId } }).then(val => {
                            let noOfStory = 1
                            let noOfStories = 1
                            if (val && val.dataValues && val.dataValues.totalStory && val.dataValues.totalStories) {
                                noOfStory = val.dataValues.totalStory + noOfStory
                                noOfStories = val.dataValues.totalStories + noOfStories
                            }
                            sequelize.model('subject').update({ totalStory: noOfStory, totalStories: noOfStories }, { where: { id: instance.dataValues.subjectId }, fields: ['totalStory', 'totalStories'] })
                        })

                    }

                }
            }
        }
    })


    // story relationship
    story.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    story.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    story.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })

    story.belongsTo(sequelize.model('subject'),
        {
            foreignKey: {
                allowNull: false
            }
        })

    story.belongsTo(sequelize.model('topic'),
        {
            foreignKey: {
                allowNull: false
            }
        })

    story.belongsTo(sequelize.model('filestore'),
        {
            as: 'thumbnailUrl',
            foreignKey: {
                name: 'thumbnail',
                allowNull: true
            }
        })
    return story
}

/**
 * @swagger
 *  components:
 *    schemas:
 *      StoryWithoutId:
 *        type: object
 *        required:
 *          - title
 *          - subjectId
 *          - topicId
 *          - active
 *          - sortNumber
 *        properties:
 *          title:
 *              type: string
 *          subjectId:
 *              type: string
 *              format: uuid
 *          topicId:
 *              type: string
 *              format: uuid
 *          active:
 *              type: boolean
 *          sortNumber:
 *              type: integer
 *              format: int32
 *          thumbnail:
 *              type: string
 *              format: uuid
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      StoryForUpdate:
 *        type: object
 *        properties:
 *          title:
 *              type: string
 *          subjectId:
 *              type: string
 *              format: uuid
 *          topicId:
 *              type: string
 *              format: uuid
 *          active:
 *              type: boolean
 *          sortNumber:
 *              type: integer
 *              format: int32
 *          thumbnail:
 *              type: string
 *              format: uuid
 *
 */




/**
 * @swagger
 *  components:
 *    schemas:
 *      StoryWithId:
 *        type: object
 *        properties:
 *          uuidIdentifier:
 *            type: uuid
 *          title:
 *              type: string
 *          subjectId:
 *              type: string
 *              format: uuid
 *          topicId:
 *              type: string
 *              format: uuid
 *          active:
 *              type: boolean
 *          sortNumber:
 *              type: integer
 *              format: int32
 *          thumbnail:
 *              type: object
 *              $ref: '#/components/schemas/ImageUrl'
 *          createdBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          updatedBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          deletedBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          createdAt:
 *              type: string
 *              format: date-time
 *          updatedAt:
 *              type: string
 *              format: date-time
 *          deletedAt:
 *              type: string
 *              format: date-time
 *          stories:
 *              type: array
 *              items:
 *                  type: string
 *                  format: uuid
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      StoryForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              recover:
 *                type: boolean
 *              active:
 *                type: boolean
 *              subjectId:
 *                type: string
 *                format: uuid
 *              topicId:
 *                type: string
 *                format: uuid
 */

