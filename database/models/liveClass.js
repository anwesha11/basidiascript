module.exports = (sequelize, type) => {
    const liveClass = sequelize.define('liveClass', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        name: {
            type: type.STRING,
            allowNull: false
        },
        url: {
            type: type.STRING,
            allowNull: false
        },
        startDate: {
            type: type.DATE,
            allowNull: false
        },
        endDate: {
            type: type.DATE,
            allowNull: false
        },
        active: {
            type: type.BOOLEAN,
            allowNull: false,
            defaultValue: true
        },
        sortNumber: {
            type: type.INTEGER,
            allowNull: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
    }, {
        underscored: true,
        freezeTableName: false,
    });

    liveClass.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    liveClass.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    liveClass.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })
    liveClass.belongsTo(sequelize.model('filestore'),
        {
            as: 'thumbnailUrl',
            foreignKey: {
                name: 'thumbnail',
                allowNull: true
            }
        })
    liveClass.belongsTo(sequelize.model('classes'), {
        as: 'class',
        foreignKey: {
            name: 'classId',
            allowNull: true
        }
    })
    liveClass.belongsTo(sequelize.model('instructor'),
        {
            foreignKey: {
                allowNull: true
            }
        })
    return liveClass;
};

/**
 * @swagger
 *  components:
 *    schemas:
 *      liveClassWithId:
 *        type: object
 *        required:
 *          - name
 *          - url
 *          - startDate
 *          - endDate
 *          - classId
 *          - sortNumber
 *          - active
 *          - instructorId
 *        properties:
 *          name:
 *            type: string
 *          classId:
 *            type: string
 *            format: uuid
 *          url:
 *            type: string
 *            format: uri
 *          thumbnail:
 *            type: string
 *            format: uuid
 *          startDate:
 *            type: string
 *            format: date
 *            example: 2017-12-31T22:00:00Z
 *          endDate:
 *            type: string
 *            format: date
 *            example: 2017-12-31T22:00:00Z
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          active:
 *            type: boolean
 *          instructorId:
 *            type: string
 *            format: uuid
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      liveClassForUpdate:
 *        type: object
 *        properties:
 *          name:
 *            type: string
 *          classId:
 *            type: string
 *            format: uuid
 *          url:
 *            type: string
 *            format: uri
 *          thumbnail:
 *            type: string
 *            format: uuid
 *          startDate:
 *            type: string
 *            format: date
 *            example: 2017-12-31T22:00:00Z
 *          endDate:
 *            type: string
 *            format: date
 *            example: 2017-12-31T22:00:00Z
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          active:
 *            type: boolean
 *          instructorId:
 *            type: string
 *            format: uuid
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      liveClassWithoutId:
 *        type: object
 *        properties:
 *          name:
 *            type: string
 *          classId:
 *            type: string
 *            format: uuid
 *          url:
 *            type: string
 *            format: uri
 *          thumbnail:
 *            type: string
 *            format: uuid
 *          startDate:
 *            type: string
 *            format: date
 *          endDate:
 *            type: string
 *            format: date
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          instructorId:
 *            type: string
 *            format: uuid
 *          active:
 *            type: boolean
 *          createdBy:
 *            type: object
 *            $ref: '#/components/schemas/CreatedBy'
 *          updatedBy:
 *            type: object
 *            $ref: '#/components/schemas/CreatedBy'
 *          deletedBy:
 *            type: object
 *            $ref: '#/components/schemas/CreatedBy'
 *          createdAt:
 *            type: string
 *            format: date
 *          updatedAt:
 *            type: string
 *            format: date
 *          deletedAt:
 *            type: string
 *            format: date
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      liveClassBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              recover:
 *                type: boolean
 *              active:
 *                type: boolean
 */