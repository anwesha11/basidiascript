const _ = require('lodash')
module.exports = (sequelize, type) => {
    const video = sequelize.define('video', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        title: {
            type: type.STRING,
            allowNull: false
        },
        urlHigh: {
            type: type.STRING,
            allowNull: false
        },
        urlMedium: {
            type: type.STRING,
            allowNull: true
        },
        urlLow: {
            type: type.STRING,
            allowNull: true
        },
        duration: {
            type: type.INTEGER,
            allowNull: true
        },
        status: {
            type: type.INTEGER,
            allowNull: false,
            validate: {
                min: 0,
                max: 1
            }
        },
        description: {
            type: type.STRING,
            allowNull: true
        },
        active: {
            type: type.BOOLEAN,
            allowNull: true,
            defaultValue: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        verticalVideo: {
            type: type.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        isMilestoneVideo: {
            type: type.BOOLEAN,
            allowNull: false
        },
        sortNumber: {
            type: type.INTEGER,
            allowNull: true,
            defaultValue: 0
        }
    }, {
        underscored: true,
        freezeTableName: true,
        hooks: {
            afterCreate: (instance, options) => {
                ////console.log('instance-----------------', instance)
                sequelize.model('topic').findOne({ where: { id: instance.dataValues.topicId } }).then(val => {
                    ////console.log('topicVal', val)
                    let totalVideos = 1
                    if (val.dataValues.totalVideos) totalVideos = val.dataValues.totalVideos + totalVideos
                    sequelize.model('topic').update({ totalVideos }, { where: { id: instance.dataValues.topicId }, fields: ['totalVideos'] })
                })
                sequelize.model('subject').findOne({ where: { id: instance.dataValues.subjectId } }).then(val => {
                    ////console.log('subjectVal', val)
                    let totalVideos = 1
                    if (val.dataValues.totalVideos) totalVideos = val.dataValues.totalVideos + totalVideos
                    sequelize.model('subject').update({ totalVideos }, { where: { id: instance.dataValues.subjectId }, fields: ['totalVideos'] })
                })
            },
            beforeBulkUpdate: async (instance, options) => {
                ////console.log('Beforfe instance-----------------', instance)

                if (_.includes(instance.fields, 'topicId') || _.includes(instance.fields, 'subjectId')) {
                    const videoData = await sequelize.model('video').findOne({ where: { uuidIdentifier: instance.where.uuidIdentifier } })

                    const topicId = videoData.dataValues.topicId
                    const subjectId = videoData.dataValues.subjectId
                    // update the video count
                    const topic = await sequelize.model('topic').findOne({ where: { id: topicId } })
                    let count = topic.dataValues.totalVideos
                    if (count && count > 0) {
                        count = count - 1
                        await sequelize.model('topic').update({ totalVideos: count }, { where: { id: topicId }, fields: ['totalVideos'] })
                    }
                    // update the video count
                    const subject = await sequelize.model('subject').findOne({ where: { id: subjectId } })
                    let count1 = subject.dataValues.totalVideos
                    if (count1 && count1 > 0) {
                        count1 = count1 - 1
                        await sequelize.model('subject').update({ totalVideos: count1 }, { where: { id: subjectId }, fields: ['totalVideos'] })
                    }
                }
            },
            afterBulkUpdate: async (instance, options) => {
                ////console.log('after instance-----------------', instance)
                if (_.includes(instance.fields, 'topicId') || _.includes(instance.fields, 'subjectId')) {

                    const topic = await sequelize.model('topic').findOne({ where: { id: instance.attributes.topicId } })
                    let count = topic.dataValues.totalVideos
                    ////console.log(count)
                    if (count) count = count + 1
                    else count = 1
                    await sequelize.model('topic').update({ totalVideos: count }, { where: { id: instance.attributes.topicId }, fields: ['totalVideos'] })

                    // update the video count
                    const subject = await sequelize.model('subject').findOne({ where: { id: instance.attributes.subjectId } })
                    let count1 = subject.dataValues.totalVideos
                    ////console.log(count1)
                    if (count1) count1 = count1 + 1
                    else count1 = 1
                    await sequelize.model('subject').update({ totalVideos: count1 }, { where: { id: instance.attributes.subjectId }, fields: ['totalVideos'] })



                }
            }
        }
    })
    // video relationship

    video.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    video.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    video.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })
    video.belongsTo(sequelize.model('subject'),
        {
            foreignKey: {
                allowNull: false
            }
        })

    video.belongsTo(sequelize.model('topic'),
        {
            foreignKey: {
                allowNull: false
            }
        })
    video.belongsTo(sequelize.model('instructor'),
        {
            foreignKey: {
                allowNull: true
            }
        })
    video.belongsTo(sequelize.model('filestore'),
        {
            as: 'thumbnailUrl',
            foreignKey: {
                name: 'thumbnail',
                allowNull: true
            }
        })
    return video
}

/**
 * @swagger
 *  components:
 *    schemas:
 *      VideoWithoutId:
 *        type: object
 *        required:
 *          - title
 *          - urlHigh
 *          - instructorId
 *          - status
 *          - subjectId
 *          - topicId
 *          - active
 *          - verticalVideo
 *          - isMilestoneVideo
 *          - sortNumber
 *        properties:
 *          title:
 *            type: string
 *          urlHigh:
 *            type: string
 *            format: uri
 *          urlMedium:
 *            type: string
 *            format: uri
 *          urlLow:
 *            type: string
 *            format: uri
 *          duration:
 *            type: integer
 *            format: int32
 *          instructorId:
 *            type: string
 *            format: uuid
 *          status:
 *            type: integer
 *            enum: [Locked, Free]
 *          description:
 *            type: string
 *          subjectId:
 *            type: string
 *            format: uuid
 *          topicId:
 *            type: string
 *            format: uuid
 *          thumbnail:
 *            type: string
 *            format: uuid
 *          active:
 *            type: boolean
 *          verticalVideo:
 *            type: boolean
 *          isMilestoneVideo:
 *            type: boolean
 *          sortNumber:
 *            type: integer
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      VideoForUpdate:
 *        type: object
 *        properties:
 *          title:
 *            type: string
 *          urlHigh:
 *            type: string
 *            format: uri
 *          urlMedium:
 *            type: string
 *            format: uri
 *          urlLow:
 *            type: string
 *            format: uri
 *          duration:
 *            type: integer
 *            format: int32
 *          instructorId:
 *            type: string
 *            format: uuid
 *          status:
 *            type: string
 *            enum: [Locked, Free]
 *          description:
 *            type: string
 *          subjectId:
 *            type: string
 *            format: uuid
 *          topicId:
 *            type: string
 *            format: uuid
 *          thumbnail:
 *            type: string
 *            format: uuid
 *          active:
 *            type: boolean
 *          verticalVideo:
 *            type: boolean
 *          isMilestoneVideo:
 *            type: boolean
 *          sortNumber:
 *            type: integer
 */


/**
* @swagger
*  components:
*    schemas:
*      VideoWithId:
*        type: object
*        properties:
*          uuidIdentifier:
*            type: uuid
*          title:
*            type: string
*          urlHigh:
*            type: string
*            format: uri
*          urlMedium:
*            type: string
*            format: uri
*          urlLow:
*            type: string
*            format: uri
*          duration:
*            type: integer
*            format: int32
*          instructorId:
*            type: string
*            format: uuid
*          status:
*            type: integer
*            enum: [Locked, Free]
*          description:
*            type: string
*          subjectId:
*            type: string
*            format: uuid
*          topicId:
*            type: string
*            format: uuid
*          createdBy:
*            type: object
*            $ref: '#/components/schemas/CreatedBy'
*          updatedBy:
*            type: object
*            $ref: '#/components/schemas/CreatedBy'
*          deletedBy:
*            type: object
*            $ref: '#/components/schemas/CreatedBy'
*          thumbnailUrl:
*            type: object
*            $ref: '#/components/schemas/ImageUrl'
*          createdAt:
*            type: string
*            format: date
*          updatedAt:
*            type: string
*            format: date
*          deletedAt:
*            type: string
*            format: date
*          active:
*            type: boolean
*          verticalVideo:
*            type: boolean
*          isMilestoneVideo:
*            type: boolean
*          sortNumber:
*            type: integer
*/
/**
 * @swagger
 *  components:
 *    schemas:
 *      VideoForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              recover:
 *                type: boolean
 *              active:
 *                type: boolean
 *              subjectId:
 *                type: string
 *                format: uuid
 *              status:
 *                type: string
 *                enum: [Locked, Free]
 *              instructorId:
 *                type: string
 *                format: uuid
 */