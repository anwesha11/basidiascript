module.exports = (sequelize, type) => {
    const instructor = sequelize.define('instructor', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        name: {
            type: type.STRING,
            allowNull: false
        },
        active: {
            type: type.BOOLEAN,
            allowNull: true,
            defaultValue: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        }
    }, {
        underscored: true,
        freezeTableName: true,
    })
    // instructor relationship

    instructor.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    instructor.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    instructor.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })
    instructor.belongsTo(sequelize.model('subject'),
        {
            foreignKey: {
                allowNull: false
            }
        })

    instructor.belongsTo(sequelize.model('filestore'),
        {
            as: 'thumbnailUrl',
            foreignKey: {
                name: 'thumbnail',
                allowNull: true
            }
        })
    return instructor
}

/**
 * @swagger
 *  components:
 *    schemas:
 *      InstructorWithoutId:
 *        type: object
 *        required:
 *          - name
 *          - subjectId
 *          - active
 *        properties:
 *          name:
 *            type: string
 *          subjectId:
 *            type: string
 *            format: uuid
 *          thumbnail:
 *            type: string
 *            format: uuid
 *          active:
 *            type: boolean
 */

/**
* @swagger
*  components:
*    schemas:
*      InstructorForUpdate:
*        type: object
*        properties:
*          name:
*            type: string
*          subjectId:
*            type: string
*            format: uuid
*          thumbnail:
*            type: string
*            format: uuid
*          active:
*            type: boolean
*/

/**
 * @swagger
 *  components:
 *    schemas:
 *      InstructorWithId:
 *        type: object
 *        properties:
 *          uuidIdentifier:
 *            type: uuid
 *          name:
 *            type: string
 *          subjectId:
 *            type: string
 *            format: uuid
 *          thumbnailUrl:
 *            type: object
 *            $ref: '#/components/schemas/ImageUrl'
 *          createdBy:
 *            type: object
 *            $ref: '#/components/schemas/CreatedBy'
 *          updatedBy:
 *            type: object
 *            $ref: '#/components/schemas/CreatedBy'
 *          deletedBy:
 *            type: object
 *            $ref: '#/components/schemas/CreatedBy'
 *          createdAt:
 *            type: string
 *            format: date
 *          updatedAt:
 *            type: string
 *            format: date
 *          deletedAt:
 *            type: string
 *            format: date
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      InstructorForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              recover:
 *                type: boolean
 *              active:
 *                type: boolean
 *              subjectId:
 *                type: string
 *                format: uuid
 */