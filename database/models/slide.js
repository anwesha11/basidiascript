module.exports = (sequelize, type) => {
    const slide = sequelize.define('slide', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        title: {
            type: type.STRING,
            allowNull: false
        },
        images: {
            type: type.JSONB,
            allowNull: false
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        isMilestoneSlide: {
            type: type.BOOLEAN,
            allowNull: false
        },
        active: {
            type: type.BOOLEAN,
            allowNull: false,
            defaultValue: true
        }
    }, {
        underscored: true,
        freezeTableName: true,
    });

    slide.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    slide.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    slide.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })
    return slide;
};


/**
 * @swagger
 *  components:
 *    schemas:
 *      SlideWithoutId:
 *        type: object
 *        required:
 *          - images
 *          - isMilestoneSlide
 *          - title
 *        properties:
 *          images:
 *            type: array
 *            items:
 *              type: object
 *              required:
 *                  - image
 *                  - caption
 *              properties:
 *                  image:
 *                      type: string
 *                      format: uuid
 *                  caption:
 *                      type: string
 *          isMilestoneSlide:
 *            type: boolean
 *          title:
 *            type: string
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      SlideForUpdate:
 *        type: object
 *        properties:
 *          images:
 *            type: array
 *            items:
 *              type: object
 *              required:
 *                  - image
 *                  - caption
 *              properties:
 *                  image:
 *                      type: string
 *                      format: uuid
 *                  caption:
 *                      type: string
 *          isMilestoneSlide:
 *            type: boolean
 *          title:
 *            type: string
 */




/**
 * @swagger
 *  components:
 *    schemas:
 *      SlideWithId:
 *        type: object
 *        properties:
 *          uuidIdentifier:
 *            type: uuid
 *          images:
 *            type: array
 *            items:
 *              type: object
 *              required:
 *                  - image
 *                  - caption
 *              properties:
 *                  image:
 *                      type: string
 *                      format: uuid
 *                  caption:
 *                      type: string
 *          isMilestoneSlide:
 *            type: boolean
 *          title:
 *            type: string
 *          createdBy:
 *            type: string
 *            format: uuid
 *          updatedBy:
 *            type: string
 *            format: uuid
 *          deletedBy:
 *            type: string
 *            format: uuid
 *          createdAt:
 *            type: string
 *            format: date
 *          updatedAt:
 *            type: string
 *            format: date
 *          deletedAt:
 *            type: string
 *            format: date
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      slideForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              recover:
 *                type: boolean
 *              active:
 *                type: boolean
 */