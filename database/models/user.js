module.exports = (sequelize, type) => {
    const user = sequelize.define('user', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        firstName: {
            type: type.STRING,
            allowNull: false
        },
        lastName: {
            type: type.STRING,
            allowNull: false
        },
        password: {
            type: type.STRING,
            allowNull: true
        },
        referralCode: {
            type: type.STRING,
            allowNull: true
        },
        country: {
            type: type.STRING,
            allowNull: true
        },
        countryPhoneCode: {
            type: type.STRING,
            allowNull: true
        },
        blocked: {
            type: type.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        firebaseToken: {
            type: type.TEXT,
            allowNull: true
        },
        city: {
            type: type.STRING,
            allowNull: true
        }
    }, {
        underscored: true,
        freezeTableName: false,
    })

    // user relationship
    user.belongsTo(sequelize.model('filestore'), {
        as: 'profilePicUrl',
        foreignKey: {
            name: 'profilePicture',
            allowNull: true
        }
    })
    user.belongsTo(sequelize.model('state'), {
        foreignKey: {
            allowNull: true
        }
    })
    user.belongsTo(sequelize.model('classes'), {
        as: 'class',
        foreignKey: {
            name: 'classId',
            allowNull: true
        }
    })
    const userApps = sequelize.define('user_apps', {
        classId: {
            type: type.INTEGER,
            allowNull: false
        }
    }, {
        underscored: true,
        freezeTableName: true,
    })
    user.belongsToMany(sequelize.model('app'),
        {
            as: 'apps',
            through: userApps,
            allowNull: false
        })
    sequelize.model('app').belongsToMany(user,
        {
            as: 'users',
            through: userApps,
            allowNull: false
        })
    return user
}

/**
 * @swagger
 *  components:
 *    schemas:
 *      UserForUpdate:
 *        type: object
 *        properties:
 *         firstName:
 *            type: string
 *         lastName:
 *            type: string
 *         country:
 *            type: string
 *         countryPhoneCode:
 *            type: string
 *         profilePicture:
 *            type: string
 *            format: uuid
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      User:
 *        type: object
 *        properties:
 *          id:
 *            type: string
 *            format: uuid
 *          firstName:
 *            type: string
 *          lastName:
 *            type: string
 *          roleId:
 *            type: string
 *            format: uuid
 *          classes:
 *            type: string
 *            format: uuid
 *          email:
 *            type: string
 *            format: email
 *          mobileNo:
 *            type: string
 *            minLength: 10
 *            maxLength: 10
 *          country:
 *            type: string
 *          countryPhoneCode:
 *            type: string
 *          profilePicture:
 *            type: string
 *            format: uuid
 */

/**
* @swagger
*  components:
*    schemas:
*      UserWithoutId:
*        type: object
*        required:
*          - firstName
*          - lastName
*          - mobileNo
*          - email
*          - password
*          - country
*          - countryPhoneCode
*          - collegeName
*        properties:
*          firstName:
*            type: string
*          lastName:
*            type: string
*          classId:
*            type: string
*            format: uuid
*          email:
*            type: string
*            format: email
*          mobileNo:
*            type: string
*            minLength: 10
*            maxLength: 10
*          country:
*            type: string
*          countryPhoneCode:
*            type: string
*          profilePicture:
*            type: string
*            format: uuid
*          password:
*            type: string
*            format: password
*          firebaseToken:
*            type: string
*          blocked:
*            type: boolean
*          collegeName:
*            type: string
*          isConsentAccepted:
*            type: boolean
*/

/**
* @swagger
*  components:
*    schemas:
*      UserWithoutPassword:
*        type: object
*        properties:
*          id:
*            type: integer
*            format: int32
*          uuidIdentifier:
*            type: uuid
*          firstName:
*            type: string
*          lastName:
*            type: string
*          role:
*            type: string
*            format: uuid
*          classes:
*            type: string
*            format: uuid
*          email:
*            type: string
*            format: email
*          mobileNo:
*            type: string
*            minLength: 10
*            maxLength: 10
*          country:
*            type: string
*          countryPhoneCode:
*            type: string
*          profilePicture:
*            type: string
*            format: uuid
*          blocked:
*            type: boolean
*          firebaseToken:
*            type: string
*/

/**
* @swagger
*  components:
*    schemas:
*      UserArray:
*        type: array
*        items:
*          type: object
*          properties:
*            uuidIdentifier:
*              type: string
*              format: uuid
*            firstName:
*              type: string
*            lastName:
*              type: string
*            role:
*              type: string
*              enum: [admin, user, content, video]
*            classes:
*              type: string
*              enum: [Pre 11, Class 11, Class 12, Post 12]
*            email:
*              type: string
*              format: email
*            mobileNo:
*              type: string
*              minLength: 10
*              maxLength: 10
*            profilePicture:
*              type: string
*              format: uuid
*            blocked:
*              type: boolean
*            firebaseToken:
*              type: string
*/


/**
 * @swagger
 *  components:
 *    schemas:
 *      CreatedBy:
 *        type: object
 *        required:
 *          - firstName
 *        properties:
 *          firstName:
 *            type: string
 *            example: Sumit
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      UserForBulkUpdate:
 *        type: object
 *        properties:
 *          blocked:
 *            type: boolean
 *          signOut:
 *            type: boolean
 *          roleId:
 *            type: string
 *            format: uuid
 */