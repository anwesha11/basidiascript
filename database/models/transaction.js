module.exports = (sequelize, type) => {
    const transaction = sequelize.define('transaction', {
        id: {
            allowNull: false,
            primaryKey: true,
            type: type.UUID
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        transactionId: {
            type: type.STRING
        },
        success: {
            type: type.BOOLEAN
        },
        amount: {
            type: type.FLOAT,
            allowNull: false
        }
    }, {
        freezeTableName: false,
        underscored: true
    });
    transaction.belongsTo(sequelize.model('user'), {
        foreignKey: {
            allowNull: false
        }
    })
    transaction.belongsTo(sequelize.model('subscription'), {
        foreignKey: {
            allowNull: false
        }
    })
    return transaction;
};

