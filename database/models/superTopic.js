const _ = require('lodash')
module.exports = (sequelize, type) => {
    const superTopic = sequelize.define('superTopic', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        name: {
            type: type.TEXT,
            allowNull: false
        },
        sortNumber: {
            type: type.INTEGER,
            allowNull: true,
        },
        active: {
            type: type.BOOLEAN,
            allowNull: true,
            defaultValue: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        totalTopic: {
            type: type.INTEGER,
            allowNull: true
        }
    }, {
        underscored: true,
        hooks: {
            afterCreate: (instance, options) => {
                ////console.log(instance, 'instance')
                sequelize.model('subject').findOne({ where: { id: instance.dataValues.subjectId } }).then(val => {
                    let noOfSuperTopics = 1
                    if (val && val.dataValues && val.dataValues.totalSuperTopic) {
                        noOfSuperTopics = val.dataValues.totalSuperTopic + noOfSuperTopics
                    }
                    sequelize.model('subject').update({ totalSuperTopic: noOfSuperTopics }, { where: { id: instance.dataValues.subjectId }, fields: ['totalSuperTopic'] })
                })

            },
            beforeBulkUpdate: async (instance, options) => {
                ////console.log('Beforfe instance-----------------', instance)
                if (_.includes(instance.fields, 'subjectId')) {
                    const superTopicData = await sequelize.model('superTopic').findOne({ where: { uuidIdentifier: instance.where.uuidIdentifier } })
                    const subjectId = superTopicData.dataValues.subjectId
                    const subject = await sequelize.model('subject').findOne({ where: { id: subjectId } })
                    let count1 = subject.dataValues.totalSuperTopic
                    if (count1 && count1 > 0) {
                        count1 = count1 - 1
                        await sequelize.model('subject').update({ totalSuperTopic: count1 }, { where: { id: subjectId }, fields: ['totalSuperTopic'] })
                    }
                }
            },
            afterBulkUpdate: async (instance, options) => {
                ////console.log('after instance-----------------', instance)
                if (_.includes(instance.fields, 'subjectId')) {

                    // update the video count
                    const subject = await sequelize.model('subject').findOne({ where: { id: instance.attributes.subjectId } })
                    let count1 = subject.dataValues.totalSuperTopic

                    if (count1) count1 = count1 + 1
                    else count1 = 1
                    await sequelize.model('subject').update({ totalSuperTopic: count1 }, { where: { id: instance.attributes.subjectId }, fields: ['totalSuperTopic'] })

                }
            }
        }
    })

    // superTopic relationship
    superTopic.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    superTopic.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    superTopic.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })
    superTopic.belongsTo(sequelize.model('subject'),
        {
            foreignKey: {
                allowNull: false
            }
        })
    superTopic.belongsTo(sequelize.model('classes'),
        {
            as: 'class',
            foreignKey: {
                name: 'classId',
                allowNull: false
            }
        })
    return superTopic
}

/**
 * @swagger
 *  components:
 *    schemas:
 *      SuperTopicWithoutId:
 *        type: object
 *        required:
 *          - name
 *          - sortNumber
 *          - classId
 *          - subjectId
 *          - active
 *        properties:
 *          name:
 *            type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          classes:
 *            type: string
 *            format: uuid
 *          subjectId:
 *            type: string
 *            format: uuid
 *          active:
 *            type: boolean
 *          topicIds:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      SuperTopicForUpdate:
 *        type: object
 *        properties:
 *          name:
 *            type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          classId:
 *            type: string
 *            format: uuid
 *          subjectId:
 *            type: string
 *            format: uuid
 *          active:
 *            type: boolean
 *          topicIds:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      SuperTopicForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              recover:
 *                  type: boolean
 *              active:
 *                  type: boolean
 */

/**
* @swagger
*  components:
*    schemas:
*      SuperTopicWithId:
*        type: object
*        properties:
*          uuidIdentifier:
*            type: uuid
*          name:
*            type: string
*          sortNumber:
*            type: integer
*            format: int32
*          classId:
*            type: string
*            format: uuid
*          subjectId:
*            type: string
*            format: uuid
*          createdBy:
*            type: object
*            $ref: '#/components/schemas/CreatedBy'
*          updatedBy:
*            type: object
*            $ref: '#/components/schemas/CreatedBy'
*          deletedBy:
*            type: object
*            $ref: '#/components/schemas/CreatedBy'
*          createdAt:
*            type: string
*            format: date
*          updatedAt:
*            type: string
*            format: date
*          deletedAt:
*            type: string
*            format: date
*/