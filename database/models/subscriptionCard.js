module.exports = (sequelize, type) => {
    const subscriptionCard = sequelize.define('subscription_card', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        title: {
            type: type.STRING,
            allowNull: false
        },
        url: {
            type: type.STRING,
            allowNull: false
        },
        active: {
            type: type.BOOLEAN,
            allowNull: false,
            defaultValue: true
        },
        sortNumber: {
            type: type.INTEGER,
            allowNull: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
    }, {
        underscored: true,
        freezeTableName: true,
    });
    subscriptionCard.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })
    subscriptionCard.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })
    subscriptionCard.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })
    subscriptionCard.belongsTo(sequelize.model('filestore'),
        {
            as: 'thumbnailUrl',
            foreignKey: {
                name: 'thumbnail',
                allowNull: true
            }
        })
    subscriptionCard.belongsTo(sequelize.model('classes'), {
        as: 'class',
        foreignKey: {
            name: 'classId',
            allowNull: true
        }
    })
    return subscriptionCard;
};