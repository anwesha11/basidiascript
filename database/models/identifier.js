module.exports = (sequelize, type) => {
    const identifier = sequelize.define('identifier', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        type: {
            type: type.INTEGER,
            allowNull: false,
            validate:{
                min:0,
                max:3
            }
        },
        identifier: {
            type: type.TEXT,
            allowNull: false,
            unique: {
                msg: 'identifier is already in use'
            }
        },
        isVerified: {
            type: type.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        underscored: true,
        freezeTableName: true,
    })

    // identifier relationship
    identifier.belongsTo(sequelize.model('user'), {
        foreignKey: {
            allowNull: false
        }
    })
    sequelize.model('user').hasMany(identifier)

    return identifier
}