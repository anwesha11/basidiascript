module.exports = (sequelize, type) => {
    const modules = sequelize.define('modules', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        name: {
            type: type.STRING,
            allowNull: false,
            unique: {
                msg: 'module name is already in use'
            }
        },
        description: {
            type: type.TEXT,
            allowNull: false
        },
        code: {
            type: type.INTEGER,
            allowNull: false,
            unique: {
                msg: 'code is already in use'
            }
        },
        sortNumber: {
            type: type.INTEGER,
            allowNull: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        }
    }, {
        underscored: true,
        freezeTableName: true
    });


    return modules;
};


/**
 * @swagger
 *  components:
 *    schemas:
 *      ModulesWithoutId:
 *        type: object
 *        required:
 *          - name
 *          - description
 *          - sortNumber
 *        properties:
 *          name:
 *            type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          description:
 *            type: string
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      ModulesForUpdate:
 *        type: object
 *        properties:
 *          name:
 *            type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          description:
 *            type: string
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      ModulesWithId:
 *        type: object
 *        properties:
 *          uuidIdentifier:
 *            type: string
 *            format: uuid
 *          name:
 *            type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          description:
 *            type: string
 *          createdAt:
 *            type: string
 *            format: date
 *          updatedAt:
 *            type: string
 *            format: date
 *          deletedAt:
 *            type: string
 *            format: date
 *          code: 
 *            type: integer
 *            format: int32
 */

