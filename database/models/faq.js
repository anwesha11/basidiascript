module.exports = (sequelize, type) => {
    const faq = sequelize.define('faq', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        question: {
            type: type.TEXT,
            allowNull: false
        },
        answer: {
            type: type.TEXT,
            allowNull: false
        },
        // category: {
        //     type: type.INTEGER,
        //     allowNull: true
        // },
        // sortNumber: {
        //     type: type.INTEGER,
        //     allowNull: true
        // },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        active: {
            type: type.BOOLEAN,
            allowNull: false,
            defaultValue: true
        }
    }, {
        underscored: true,
        freezeTableName: true,
    });

    faq.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    faq.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    faq.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })

    faq.belongsTo(sequelize.model('classes'),
        {
            as: 'class',
            foreignKey: {
                name: 'classId',
                allowNull: true
            }
        })
    return faq;
};


/**
 * @swagger
 *  components:
 *    schemas:
 *      FaqWithoutId:
 *        type: object
 *        required:
 *          - question
 *          - answer
 *          - category
 *          - active
 *          - classId
 *        properties:
 *          question:
 *            type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          active:
 *            type: boolean
 *          answer:
 *            $ref: '#/components/schemas/FaqAnswer'
 *          category:
 *            type: string
 *            enum: [General Question,MCQ Lessons,Test Series,Video Classes,Subscription & Refund,Help & Support]
 *          answerImage:
 *            type: string
 *            format: uuid
 *          classId:
 *            type: string
 *            format: uuid
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      FaqForUpdate:
 *        type: object
 *        properties:
 *          question:
 *            type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          active:
 *            type: boolean
 *          answer:
 *            $ref: '#/components/schemas/FaqAnswer'
 *          category:
 *            type: string
 *            enum: [General Question,MCQ Lessons,Test Series,Video Classes,Subscription & Refund,Help & Support]
 *          answerImage:
 *            type: string
 *            format: uuid
 *          classId:
 *            type: string
 *            format: uuid
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      FaqForBulkUpdate:
 *        type: object
 *        properties:
 *          recover:
*             type: boolean
 *          active:
 *            type: boolean
 *          category:
 *            type: string
 *            enum: [General Question,MCQ Lessons,Test Series,Video Classes,Subscription & Refund,Help & Support]
 */



/**
 * @swagger
 *  components:
 *    schemas:
 *      FaqWithId:
 *        type: object
 *        properties:
 *          uuidIdentifier:
 *            type: uuid
 *          question:
 *            type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          active:
 *            type: boolean
 *          answer:
 *            $ref: '#/components/schemas/FaqAnswer'
 *          category:
 *            type: string
 *            enum: [General Question,MCQ Lessons,Test Series,Video Classes,Subscription & Refund,Help & Support]
 *          createdBy:
 *            type: string
 *            format: uuid
 *          updatedBy:
 *            type: string
 *            format: uuid
 *          deletedBy:
 *            type: string
 *            format: uuid
 *          createdAt:
 *            type: string
 *            format: date
 *          updatedAt:
 *            type: string
 *            format: date
 *          deletedAt:
 *            type: string
 *            format: date
 *          answerImageUrl:
 *            $ref: '#/components/schemas/ImageUrl'
 *          classId:
 *            type: string
 *            format: uuid
 */


/**
* @swagger
*  components:
*    schemas:
*      FaqAnswer:
*        type: array
*        items:
*          type: object
*          required:
*              - contentType
*              - contentData
*          properties:
*              contentType:
*                  type: string
*              contentData:
*                  type: string
*
*/