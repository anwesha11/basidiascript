module.exports = (sequelize, type) => {
    const milestone = sequelize.define('milestone', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        title: {
            type: type.TEXT,
            allowNull: false
        },
        description: {
            type: type.TEXT,
            allowNull: false
        },
        content: {
            type: type.JSONB,
            allowNull: false
        },
        active: {
            type: type.BOOLEAN,
            allowNull: false,
            defaultValue: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        sortNumber: {
            type: type.INTEGER,
            allowNull: true
        }
    }, {
        underscored: true,
        freezeTableName: true
    })

    milestone.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    milestone.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    milestone.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })


    milestone.belongsTo(sequelize.model('story'),
        {
            foreignKey: {
                allowNull: false
            }
        })

    sequelize.model('story').hasMany(milestone)

    return milestone
}


/**
 * @swagger
 *  components:
 *    schemas:
 *      MilestoneWithoutId:
 *        type: object
 *        required:
 *          - title
 *          - active
 *          - content
 *          - description
 *          - storyId
 *        properties:
 *          title:
 *            type: string
 *          description:
 *            type: string
 *          active:
 *            type: boolean
 *          sortNumber:
 *            type: integer
 *          storyId:
 *            type: string
 *            format: uuid
 *          content:
 *            type: array
 *            items:
 *              type: object
 *              required:
 *                  - type
 *                  - uuidIdentifier
 *                  - sortNumber
 *              properties:
 *                  type:
 *                    type: string
 *                    enum: [mcq, key, video, fillInTheBlank, arrangeTheFollowing, multipleCorrectChoiceQuestion, slide, q&a]
 *                  uuidIdentifier:
 *                    type: string
 *                    format: uuid
 *                  sortNumber:
 *                    type: integer
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      MilestoneForUpdate:
 *        type: object
 *        properties:
 *          title:
 *            type: string
 *          description:
 *            type: string
 *          storyId:
 *            type: string
 *            format: uuid
 *          active:
 *            type: boolean
 *          sortNumber:
 *            type: integer
 *          content:
 *            type: array
 *            items:
 *              type: object
 *              required:
 *                  - type
 *                  - uuidIdentifier
 *                  - sortNumber
 *              properties:
 *                  type:
 *                    type: string
 *                    enum: [mcq, key, video, fillInTheBlank, arrangeTheFollowing, multipleCorrectChoiceQuestion, slide, q&a]
 *                  uuidIdentifier:
 *                    type: string
 *                    format: uuid
 *                  sortNumber:
 *                    type: integer
 */





/**
 * @swagger
 *  components:
 *    schemas:
 *      MilestoneWithId:
 *        type: object
 *        properties:
 *          uuidIdentifier:
 *            type: uuid
 *          title:
 *            type: string
 *          description:
 *            type: string
 *          storyId:
 *            type: string
 *            format: uuid
 *          createdBy:
 *            type: string
 *            $ref: '#/components/schemas/CreatedBy'
 *          updatedBy:
 *            type: string
 *            $ref: '#/components/schemas/CreatedBy'
 *          deletedBy:
 *            type: string
 *            $ref: '#/components/schemas/CreatedBy'
 *          deletedAt:
 *            type: string
 *            format: date
 *          updatedAt:
 *            type: string
 *            format: date
 *          createdAt:
 *            type: string
 *            format: date
 *          active:
 *            type: boolean
 *          sortNumber:
 *            type: integer
 *          content:
 *            type: array
 *            items:
 *              type: object
 *              required:
 *                  - type
 *                  - uuidIdentifier
 *                  - sortNumber
 *              properties:
 *                  type:
 *                    type: string
 *                    enum: [mcq, key, video, fillInTheBlank, arrangeTheFollowing, multipleCorrectChoiceQuestion, slide, q&a]
 *                  data:
 *                    type: object
 *                  sortNumber:
 *                    type: integer
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      MilestoneForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              recover:
 *                type: boolean
 *              active:
 *                type: boolean
 */