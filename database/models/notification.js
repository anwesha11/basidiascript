module.exports = (sequelize, type) => {
    const notification = sequelize.define('notification', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        notificationType: {
            type: type.INTEGER,
            allowNull: false
        },
        title: {
            type: type.STRING,
            allowNull: false
        },
        subTitle: {
            type: type.STRING,
            allowNull: false
        },
        moduleType: {
            type: type.INTEGER,
            allowNull: true
        },
        moduleId: {
            type: type.STRING,
            allowNull: true
        },
        linkType: {
            type: type.INTEGER,
            allowNull: false
        },
    }, {
        underscored: true,
        freezeTableName: true,
    });

    notification.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    notification.belongsTo(sequelize.model('filestore'),
        {
            as: 'imageUrl',
            foreignKey: {
                name: 'image',
                allowNull: true
            }
        })
    return notification;
};


/**
 * @swagger
 *  components:
 *    schemas:
 *      NotificationWithoutId:
 *        type: object
 *        required:
 *          - title
 *          - subTitle
 *          - moduleType
 *          - moduleId
 *          - linkType
 *        properties:
 *          title:
 *            type: string
 *          subTitle:
 *            type: string
 *          moduleType:
 *            type: string
 *            enum: ['test']
 *          moduleId:
 *            type: string
 *            format: uuid
 *          linkType:
 *            type: string
 *            enum: ['openApp','deepLink']
 *          image: 
 *            type: string
 *            format: uuid
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      NotificationWithId:
 *        type: object
 *        properties:
 *          uuidIdentifier:
 *            type: uuid
 *          title:
 *            type: string
 *          subTitle:
 *            type: string
 *          notificationType:
 *            type: string
 *            enum: ['Notification-for-All']
 *          module:
 *            type: string
 *            enum: ['test']
 *          moduleId:
 *            type: string
 *            format: uuid
 *          linkType:
 *            type: string
 *            enum: ['openApp','deepLink']
 *          createdBy:
 *            type: string
 *            format: uuid
 *          createdAt:
 *            type: string
 *            format: date
 *          updatedAt:
 *            type: string
 *            format: date
 *          deletedAt:
 *            type: string
 *            format: date
 *          imageUrl:
 *            $ref: '#/components/schemas/ImageUrl'
 */

