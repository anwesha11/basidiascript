module.exports = (sequelize, type) => {
    const reference = sequelize.define('reference', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        title: {
            type: type.TEXT,
            allowNull: false
        },
        active: {
            type: type.BOOLEAN,
            allowNull: true,
            defaultValue: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        }
    }, {
        underscored: true,
        freezeTableName: true,
    })
    // reference relationship
    reference.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    reference.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    reference.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })
    reference.belongsTo(sequelize.model('filestore'),
        {
            as: 'imageUrl',
            foreignKey: {
                name: 'image',
                allowNull: true
            }
        })
    return reference
}

/**
 * @swagger
 *  components:
 *    schemas:
 *      ReferenceWithoutId:
 *        type: object
 *        required:
 *          - title
 *          - active
 *        properties:
 *          title:
 *            type: string
 *          active:
 *            type: boolean
 *          image:
 *            type: string
 *            format: uuid
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      ReferenceForUpdate:
 *        type: object
 *        properties:
 *          title:
 *            type: string
 *          active:
 *            type: boolean
 *          image:
 *            type: string
 *            format: uuid
 */

/**
* @swagger
*  components:
*    schemas:
*      ReferenceWithId:
*        type: object
*        properties:
*          uuidIdentifier:
*            type: uuid
*          title:
*            type: string
*          createdBy:
*            type: object
*            $ref: '#/components/schemas/CreatedBy'
*          updatedBy:
*            type: object
*            $ref: '#/components/schemas/CreatedBy'
*          deletedBy:
*            type: object
*            $ref: '#/components/schemas/CreatedBy'
*          image:
*            type: object
*            $ref: '#/components/schemas/ImageUrl'
*          createdAt:
*            type: string
*            format: date
*          updatedAt:
*            type: string
*            format: date
*          deletedAt:
*            type: string
*            format: date
*          active:
*            type: boolean
*/

/**
* @swagger
*  components:
*    schemas:
*      ReferenceForInputForMcq:
*        type: object
*        properties:
*          uuidIdentifier:
*            type: string
*            format: uuid
*          description:
*            type: string
*/

/**
* @swagger
*  components:
*    schemas:
*      ReferenceForInput:
*        type: object
*        properties:
*          uuidIdentifier:
*            type: string
*            format: uuid
*          description:
*            type: string
*          sortNumber:
*            type: number
*            format: int32
*/

/**
 * @swagger
 *  components:
 *    schemas:
 *      ReferenceForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              recover:
 *                type: boolean
 *              active:
 *                type: boolean
 */