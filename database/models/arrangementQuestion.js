const _ = require('lodash')
module.exports = (sequelize, type) => {
    const arrangementQuestion = sequelize.define('arrangementQuestion', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        question: {
            type: type.TEXT,
            allowNull: false
        },
        explanation: {
            type: type.JSONB,
            allowNull: true,
        },
        options: {
            type: type.JSONB,
            allowNull: false,
        },
        questionHtml: {
            type: type.TEXT,
            allowNull: false
        },
        active: {
            type: type.BOOLEAN,
            allowNull: true,
            defaultValue: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        sortNumber: {
            type: type.INTEGER,
            allowNull: true
        },
        isMilestoneArrangeTheFollowing: {
            type: type.BOOLEAN,
            allowNull: false
        }
    }, {
        underscored: true,
        hooks: {
            afterCreate: (instance, options) => {
                ////console.log('instance-----------------', instance)
                sequelize.model('topic').findOne({ where: { id: instance.dataValues.topicId } }).then(val => {
                    ////console.log('topicVal', val)
                    let totalAtf = 1
                    if (val.dataValues.totalAtf) totalAtf = val.dataValues.totalAtf + totalAtf
                    sequelize.model('topic').update({ totalAtf }, { where: { id: instance.dataValues.topicId }, fields: ['totalAtf'] })
                })
                sequelize.model('subject').findOne({ where: { id: instance.dataValues.subjectId } }).then(val => {
                    let noOfAtfs = 1
                    if (val && val.dataValues && val.dataValues.totalAtf) {
                        noOfAtfs = val.dataValues.totalAtf + noOfAtfs
                    }
                    sequelize.model('subject').update({ totalAtf: noOfAtfs }, { where: { id: instance.dataValues.subjectId }, fields: ['totalAtf'] })
                })
            },
            beforeBulkUpdate: async (instance, options) => {
                ////console.log('Beforfe instance-----------------', instance)

                if (_.includes(instance.fields, 'topicId')) {
                    const arrangementQuestionData = await sequelize.model('arrangementQuestion').findOne({ where: { uuidIdentifier: instance.where.uuidIdentifier } })

                    const topicId = arrangementQuestionData.dataValues.topicId

                    // update the arrangementQuestion count
                    const topic = await sequelize.model('topic').findOne({ where: { id: topicId } })
                    let count = topic.dataValues.totalAtf
                    if (count && count > 0) {
                        count = count - 1
                        await sequelize.model('topic').update({ totalAtf: count }, { where: { id: topicId }, fields: ['totalAtf'] })
                    }
                }
                if (_.includes(instance.fields, 'subjectId')) {
                    const arrangeData = await sequelize.model('arrangementQuestion').findOne({ where: { uuidIdentifier: instance.where.uuidIdentifier } })
                    const subjectId = arrangeData.dataValues.subjectId
                    const subject = await sequelize.model('subject').findOne({ where: { id: subjectId } })
                    let count1 = subject.dataValues.totalAtf
                    if (count1 && count1 > 0) {
                        count1 = count1 - 1
                        await sequelize.model('subject').update({ totalAtf: count1 }, { where: { id: subjectId }, fields: ['totalAtf'] })
                    }
                }

            },
            afterBulkUpdate: async (instance, options) => {
                ////console.log('after instance-----------------', instance)
                if (_.includes(instance.fields, 'topicId')) {

                    const topic = await sequelize.model('topic').findOne({ where: { id: instance.attributes.topicId } })
                    let count = topic.dataValues.totalAtf
                    ////console.log(count)
                    if (count) count = count + 1
                    else count = 1
                    await sequelize.model('topic').update({ totalAtf: count }, { where: { id: instance.attributes.topicId }, fields: ['totalAtf'] })
                    if (_.includes(instance.fields, 'subjectId')) {

                        // update the subject count
                        const subject = await sequelize.model('subject').findOne({ where: { id: instance.attributes.subjectId } })
                        let count1 = subject.dataValues.totalAtf

                        if (count1) count1 = count1 + 1
                        else count1 = 1
                        await sequelize.model('subject').update({ totalAtf: count1 }, { where: { id: instance.attributes.subjectId }, fields: ['totalAtf'] })

                    }
                }
            }
        }
    })


    // arrangementQuestion relationship
    arrangementQuestion.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    arrangementQuestion.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    arrangementQuestion.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })

    arrangementQuestion.belongsTo(sequelize.model('subject'),
        {
            foreignKey: {
                allowNull: false
            }
        })

    arrangementQuestion.belongsTo(sequelize.model('topic'),
        {
            foreignKey: {
                allowNull: false
            }
        })

    const arrangement_hashtag = sequelize.define('arrangement_hashtag', {

    },
        {
            underscored: true,
            freezeTableName: true,
            hooks: {
                afterCreate: async (instance, options) => {
                    ////console.log(instance, 'instance')
                    sequelize.model('hashtag').findOne({ where: { id: instance.dataValues.hashtagId } }).then(val => {
                        let noOfAtfs = 1
                        if (val && val.dataValues && val.dataValues.totalAtf) {
                            noOfAtfs = val.dataValues.totalAtf + noOfAtfs
                        }
                        sequelize.model('hashtag').update({ totalAtf: noOfAtfs }, { where: { id: instance.dataValues.hashtagId }, fields: ['totalAtf'] })
                    })
                },
                beforeBulkDestroy: async (instance, options) => {
                    ////console.log(instance, 'before destroy')
                    const mcqHashTagData = await sequelize.model('arrangement_hashtag').findAll({ where: { arrangementQuestionId: instance.where.arrangement_question_id } })
                    if (mcqHashTagData.length > 0) {
                        mcqHashTagData.forEach(async val => {
                            let hashTagId = await sequelize.model('hashtag').findOne({
                                where: {
                                    id: val.hashtagId
                                }
                            })
                            let count1 = hashTagId.dataValues.totalAtf
                            if (count1 && count1 > 0) {
                                count1 = count1 - 1
                                await sequelize.model('hashtag').update({ totalAtf: count1 }, { where: { id: val.hashtagId }, fields: ['totalAtf'] })
                            }

                        })
                    }
                }
            }
        })

    arrangementQuestion.belongsToMany(sequelize.model('hashtag'),
        {
            as: 'hashtags',
            through: arrangement_hashtag
        })

    const arrangement_reference = sequelize.define('arrangement_reference', {
        description: {
            type: type.TEXT,
            allowNull: true
        }
    }, {
        underscored: true,
        freezeTableName: true,
    })

    arrangementQuestion.belongsToMany(sequelize.model('reference'), {
        as: 'references',
        through: arrangement_reference
    })

    const arrangement_key = sequelize.define('arrangement_key', {

    }, {
        underscored: true,
        freezeTableName: true,
        hooks: {
            afterCreate: (instance, options) => {
                ////console.log(instance, 'instance')
                sequelize.model('key').findOne({ where: { id: instance.dataValues.keyId } }).then(val => {
                    let noOfAtf = 1
                    if (val && val.dataValues && val.dataValues.totalAtf) {
                        noOfAtf = val.dataValues.totalAtf + noOfAtf
                    }
                    sequelize.model('key').update({ totalAtf: noOfAtf }, { where: { id: instance.dataValues.keyId }, fields: ['totalAtf'] })
                })
            },
            beforeBulkDestroy: async (instance, options) => {
                ////console.log(instance, 'before destroy', options)
                const arrangementQuestionKeyData = await sequelize.model('arrangement_key').findAll({ where: { arrangementQuestionId: instance.where.arrangement_question_id } })
                // ////console.log(arrangementQuestionKeyData)


                if (arrangementQuestionKeyData.length > 0) {
                    arrangementQuestionKeyData.forEach(async keyData => {
                        ////console.log(keyData);
                        const key = await sequelize.model('key').findOne({ where: { id: keyData.dataValues.keyId } })
                        let count = key.dataValues.totalAtf;
                        ////console.log(count);
                        if (count) {
                            count = count - 1
                        }
                        await sequelize.model('key').update({ totalAtf: count }, { where: { id: keyData.dataValues.keyId }, fields: ['totalAtf'] })

                    })

                }

            }



        }
    })

    arrangementQuestion.belongsToMany(sequelize.model('key'), {
        as: 'relatedKeys',
        through: arrangement_key
    })


    // arrangementQuestion.belongsToMany(sequelize.model('key'), {
    //     as: 'relatedKeys',
    //     through: 'arrangement_key'
    // })
    arrangementQuestion.belongsTo(sequelize.model('filestore'),
        {
            as: 'questionImageUrl',
            foreignKey: {
                name: 'questionImage',
                allowNull: true
            }
        })
    return arrangementQuestion
}

/**
 * @swagger
 *  components:
 *    schemas:
 *      ArrangementQuestionWithoutId:
 *        type: object
 *        required:
 *          - question
 *          - subjectId
 *          - topicId
 *          - options
 *          - questionHtml
 *          - active
 *          - isMilestoneArrangeTheFollowing
 *          - explanation
 *        properties:
 *          question:
 *              type: string
 *          subjectId:
 *              type: string
 *              format: uuid
 *          topicId:
 *              type: string
 *              format: uuid
 *          options:
 *              type: array
 *              items:
 *                 $ref: '#/components/schemas/ArrangementQuestionOption'
 *          questionHtml:
 *              type: string
 *          explanation:
 *              type: array
 *              items:
 *                  type: object
 *                  $ref: '#/components/schemas/MccqExplanation'
 *          active:
 *              type: boolean
 *          sortNumber:
 *              type: integer
 *              format: int32
 *          isMilestoneArrangeTheFollowing:
 *              type: boolean
 *          references:
 *            type: array
 *            items:
 *              type: object
 *              $ref: '#/components/schemas/ReferenceForInputForMcq'
 *          hashtags:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          relatedKeys:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          questionImage:
 *            type: string
 *            format: uuid
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      ArrangementQuestionForUpdate:
 *        type: object
 *        properties:
 *          question:
 *              type: string
 *          subjectId:
 *              type: string
 *              format: uuid
 *          topicId:
 *              type: string
 *              format: uuid
 *          options:
 *              type: array
 *              items:
 *                  $ref: '#/components/schemas/ArrangementQuestionOption'
 *          questionHtml:
 *              type: string
 *          active:
 *            type: boolean
 *          sortNumber:
 *              type: integer
 *              format: int32
 *          isMilestoneArrangeTheFollowing:
 *              type: boolean
 *          references:
 *            type: array
 *            items:
 *              type: object
 *              $ref: '#/components/schemas/ReferenceForInputForMcq'
 *          hashtags:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          relatedKeys:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          explanation:
 *              type: array
 *              items:
 *                  type: object
 *                  $ref: '#/components/schemas/MccqExplanation'
 *          questionImage:
 *            type: string
 *            format: uuid
 */




/**
 * @swagger
 *  components:
 *    schemas:
 *      ArrangementQuestionWithId:
 *        type: object
 *        properties:
 *          uuidIdentifier:
 *            type: uuid
 *          question:
 *              type: string
 *          subjectId:
 *              type: string
 *              format: uuid
 *          topicId:
 *              type: string
 *              format: uuid
 *          options:
 *              type: array
 *              items:
 *                  $ref: '#/components/schemas/ArrangementQuestionOption'
 *          questionHtml:
 *              type: string
 *          createdBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          updatedBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          deletedBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          createdAt:
 *              type: string
 *              format: date-time
 *          updatedAt:
 *              type: string
 *              format: date-time
 *          deletedAt:
 *              type: string
 *              format: date-time
 *          active:
 *            type: boolean
 *          sortNumber:
 *              type: integer
 *              format: int32
 *          isMilestoneArrangeTheFollowing:
 *              type: boolean
 *          references:
 *            type: array
 *            items:
 *              type: object
 *              $ref: '#/components/schemas/ReferenceForInputForMcq'
 *          hashtags:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          relatedKeys:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          explanation:
 *              type: array
 *              items:
 *                  type: object
 *                  $ref: '#/components/schemas/MccqExplanation'
 *          questionImageUrl:
 *             type: object
 *             $ref: '#/components/schemas/ImageUrl'
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      ArrangementQuestionOption:
 *        type: object
 *        required:
 *          - optionData
 *          - optionDataHtml
 *          - position
 *          - id
 *        properties:
 *          optionData:
 *              type: string
 *          position:
 *              type: integer
 *              format: int32
 *          optionDataHtml:
 *              type: string
 *          id:
 *              type: integer
 *              format: int32
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      ArrangementQuestionForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              active:
 *                type: boolean
 *              subjectId:
 *                type: string
 *                format: uuid
 *              topicId:
 *                type: string
 *                format: uuid
 */