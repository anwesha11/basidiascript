module.exports = (sequelize, type) => {
    const subject = sequelize.define('subject', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        name: {
            type: type.TEXT,
            allowNull: false,
            unique: {
                msg: 'name is already in use'
            }
        },
        active: {
            type: type.BOOLEAN,
            allowNull: true,
            defaultValue: true
        },
        sortNumber: {
            type: type.INTEGER,
            allowNull: true,
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        code: {
            type: type.INTEGER,
            allowNull: false,
            unique: {
                msg: 'code is already in use'
            }
        },
        totalVideos: {
            type: type.INTEGER,
            allowNull: true,
        },
        totalTest: {
            type: type.INTEGER,
            allowNull: true,
        },
        totalStory: {
            type: type.INTEGER,
            allowNull: true,
        },
        totalQna: {
            type: type.INTEGER,
            allowNull: true,
        },
        totalMcq: {
            type: type.INTEGER,
            allowNull: true,
        },
        totalMccq: {
            type: type.INTEGER,
            allowNull: true,
        },
        totalKeys: {
            type: type.INTEGER,
            allowNull: true,
        },
        totalFitb: {
            type: type.INTEGER,
            allowNull: true,
        },
        totalAtf: {
            type: type.INTEGER,
            allowNull: true,
        },
        totalTopics: {
            type: type.INTEGER,
            allowNull: true,
        },
        totalSuperTopic: {
            type: type.INTEGER,
            allowNull: true,
        }
    }, {
        underscored: true,
        freezeTableName: true
    })
    // subject relationship
    subject.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    subject.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    subject.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })

    subject.belongsToMany(sequelize.model('classes'),
        {
            as: 'class',
            through: 'class_subjects',
            allowNull: false
        })
    sequelize.model('classes').belongsToMany(subject,
        {
            as: 'subjects',
            through: 'class_subjects',
            allowNull: false
        })
    subject.belongsTo(sequelize.model('filestore'),
        {
            as: 'thumbnailUrl',
            foreignKey: {
                name: 'thumbnail',
                allowNull: true
            }
        })
    return subject
}

/**
 * @swagger
 *  components:
 *    schemas:
 *      SubjectWithoutId:
 *        type: object
 *        required:
 *          - name
 *          - active
 *        properties:
 *          name:
 *            type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          active:
 *            type: boolean
 *          thumbnail:
 *            type: string
 *            format: uuid
 */
/**
 * @swagger
 *  components:
 *    schemas:
 *      SubjectForUpdate:
 *        type: object
 *        properties:
 *          name:
 *            type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          active:
 *            type: boolean
 *          thumbnail:
 *            type: string
 *            format: uuid
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      SubjectForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              recover:
 *                  type:boolean
 *              active:
 *                  type: boolean
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      SubjectWithId:
 *        type: object
 *        properties:
 *          uuidIdentifier:
 *            type: uuid
 *          name:
 *            type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          createdBy:
 *            type: string
 *            format: uuid
 *          thumbnailUrl:
 *            type: object
 *            $ref: '#/components/schemas/ImageUrl'
 *          updatedBy:
 *            type: string
 *            format: uuid
 *          deletedBy:
 *            type: string
 *            format: uuid
 *          createdAt:
 *            type: string
 *            format: date
 *          updatedAt:
 *            type: string
 *            format: date
 *          deletedAt:
 *            type: string
 *            format: date
 */