
module.exports = (sequelize, type)=>{
    const consent = sequelize.define("consent",{
                    id:{
                        type: type.INTEGER,
                        primaryKey: true,
                        allowNull: false,
                        autoIncrement: true
                    },
                    uuidIdentifier: {
                        type: type.UUID,
                        defaultValue: type.UUIDV4,
                        allowNull: false
                    },
                    isAccepted:{
                        type:type.BOOLEAN,
                        allowNull: false,
                        defaultValue:false
                    },
                    appName:{
                        type:type.STRING,
                        allowNull: false
                    }
    },{
        underscored: true,
        freezeTableName: true
    });
    sequelize.model('user').hasMany(consent)
    consent.belongsTo(sequelize.model('user'),{
        foreignKey: {
            allowNull: false
        }
    })

    
    return consent

}