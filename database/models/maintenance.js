module.exports = (sequelize, type) => {
    const maintenance = sequelize.define('maintenance', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        to: {
            type: type.DATE,
            allowNull: false,
        },
        from: {
            type: type.DATE,
            allowNull: false,
        },
        type: {
            type: type.ENUM('admin', 'app', 'both'),
            allowNull: false
        }
    }, {
        underscored: true,
        freezeTableName: true
    })
    return maintenance
}

/**
 * @swagger
 *  components:
 *    schemas:
 *      MaintenanceWithoutId:
 *        type: object
 *        required:
 *          - to
 *          - from
 *          - type
 *        properties:
 *          to:
 *            type: string
 *            format: date-time
 *          from:
 *            type: string
 *            format: date-time
 *          type:
 *            type: string
 *            enum: [app,admin,both]
 */
