module.exports = (sequelize, type)=>{
    const leaderBoard = sequelize.define('leaderBoard', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        name:{
            type: type.STRING,
            allowNull: true
        },
        score:{
            type: type.STRING,
            allowNull: true
        },
        rank:{
            type: type.STRING,
            allowNull: true
        },
        sortNumber:{
            type: type.INTEGER,
            allowNull: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        }
    }, {
        underscored: true,
        freezeTableName: false
    })

    leaderBoard.belongsTo(sequelize.model('user'),{
        as:'creationUser',
        foreignKey:{
            name:'createdBy',
            allowNull: false
        }
    })

    leaderBoard.belongsTo(sequelize.model('user'),{
        as:'updationUser',
        foreignKey:{
            name:'updatedBy',
            allowNull: false
        }
    })

    leaderBoard.belongsTo(sequelize.model('user'),{
        as:'deletionUser',
        foreignKey:{
            name:'deletedBy',
            allowNull: true
        }
    })

    leaderBoard.belongsTo(sequelize.model('test'),{
        foreignKey:{
            allowNull: false
        }
    })

    leaderBoard.belongsTo(sequelize.model('filestore'),
        {
            as: 'thumbnailUrl',
            foreignKey: {
                name: 'thumbnail',
                allowNull: true
            }
        })

    return leaderBoard

}

/**
 * @swagger
 *  components:
 *    schemas:
 *      leaderboardWithoutId:
 *        type: object
 *        required:
 *          - name
 *          - score
 *          - rank
 *          - sortNumber
 *          - testId
 *          - thumbnail
 *        properties:
 *          testId:
 *              type: string
 *              format: uuid
 *          name:
 *              type: string
 *              format: uuid
 *          score:
 *              type: string
 *          rank:
 *              type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          thumbnail:
 *            type: string
 *            format: uuid
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      leaderBoardForUpdate:
 *        type: object
 *        required:
 *          - name
 *          - score
 *          - rank
 *          - sortNumber
 *          - testId
 *          - thumbnail
 *        properties:
 *          testId:
 *              type: string
 *              format: uuid
 *          name:
 *              type: string
 *              format: uuid
 *          score:
 *              type: string
 *          rank:
 *              type: string
 *          sortNumber:
 *            type: integer
 *            format: int32
 *          thumbnail:
 *            type: string
 *            format: uuid
 */




/**
 * @swagger
 *  components:
 *    schemas:
 *      leaderBoardWithId:
 *        type: object
 *        properties:
 *          testId:
 *              type: string
 *              format: uuid
 *          name:
 *              type: string
 *              format: byte
 *          score:
 *              type: string
 *              format: byte
 *          rank:
 *              type: string
 *              format: byte
 *          sortNumber:
 *              type: integer
 *              format: int32
 *          createdBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          updatedBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          deletedBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 */


