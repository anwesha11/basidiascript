module.exports = (sequelize, type) => {
    const notes = sequelize.define('notes', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        type: {
            type: type.INTEGER,
            allowNull: true
        },
        content: {
            type: type.TEXT,
            allowNull: true
        },
        contentSize: {
            type: type.INTEGER,
            allowNull: false
        },
        mimeType: {
            type: type.TEXT,
            allowNull: true
        },
        name: {
            type: type.TEXT,
            allowNull: true
        },
        active: {
            type: type.BOOLEAN,
            allowNull: false,
            defaultValue: true
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        pagesCount: {
            type: type.INTEGER,
            defaultValue: 0
        }
    }, {
        underscored: true,
        freezeTableName: true
    })

    notes.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    notes.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    notes.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })
    notes.belongsTo(sequelize.model('subject'),
        {
            foreignKey: {
                allowNull: true
            }
        })
    notes.belongsTo(sequelize.model('topic'),
        {
            foreignKey: {
                allowNull: true
            }
        })
    notes.belongsTo(sequelize.model('questionAndAnswer'),
        {
            foreignKey: {
                name: 'qnaId',
                allowNull: false
            }
        })

        sequelize.model('questionAndAnswer').hasMany(notes,
            {
                foreignKey: {
                    name: 'qnaId',
                    allowNull: false
                }
            })
    return notes
}


/**
 * @swagger
 *  components:
 *    schemas:
 *      NotesWithoutId:
 *        type: object
 *        required:
 *          - type
 *          - content
 *          - active
 *          - qnaId
 *        properties:
 *          content:
 *            type: string
 *          type:
 *            type: string
 *            enum: [text, image, document]
 *          active:
 *            type: boolean
 *          qnaId:
 *            type: string
 *            format: uuid
 */


