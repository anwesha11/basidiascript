module.exports = (sequelize, type) => {
    const college = sequelize.define(
        "college",
        {
            id: {
                type: type.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            uuidIdentifier: {
                type: type.UUID,
                defaultValue: type.UUIDV4,
                allowNull: false
            },
            city: {
                type: type.STRING,
                allowNull: true
            },
            collegeName: {
                type: type.STRING,
                allowNull: false
            },
            active: {
                type: type.BOOLEAN,
                allowNull: false,
                defaultValue: true
            },
            sortNumber: {
                type: type.INTEGER,
                allowNull: true
            },
            deletedAt: {
                type: type.DATE,
                allowNull: true
            }
        },
        {
            underscored: true,
            freezeTableName: true
        }
    );
    // college relationship
    college.belongsTo(sequelize.model('app'),
        {
            foreignKey: {
                allowNull: false,
            }
        })
    college.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })
    college.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })
    college.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })
    college.belongsTo(sequelize.model("state"), {
        as: "state",
        foreignKey: {
            allowNull: true
        }
    });
    college.belongsToMany(sequelize.model('user'),{
        as: 'students',
        through: 'college_student',
        allowNull: false
    })
    return college;
};


/**
 * @swagger
 *  components:
 *    schemas:
 *      CollegeWithId:
 *        type: object
 *        required:
 *          - city
 *          - collegeName
 *          - stateId
 *          - active
 *          - sortNumber
 *        properties:
 *          city:
 *            type: string
 *          collegeName:
 *            type: string
 *          stateId:
 *            type: string
 *            format: uuid
 *          active:
 *            type: boolean
 *          sortNumber:
 *            type: number
 *            format: int32
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      CollegeForUpdate:
 *        type: object
 *        required:
 *          - city
 *          - collegeName
 *          - stateId
 *          - active
 *          - sortNumber
 *        properties:
 *          city:
 *            type: string
 *          collegeName:
 *            type: string
 *          stateId:
 *            type: string
 *            format: uuid
 *          active:
 *            type: boolean
 *          sortNumber:
 *            type: number
 *            format: int32
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      CollegeWithoutId:
 *        type: object
 *        properties:
 *          city:
 *            type: string
 *          collegeName:
 *            type: string
 *          stateId:
 *            type: string
 *            format: uuid
 *          active:
 *            type: boolean
 *          sortNumber:
 *            type: number
 *            format: int32
 *          createdBy:
 *            type: object
 *            $ref: '#/components/schemas/CreatedBy'
 *          updatedBy:
 *            type: object
 *            $ref: '#/components/schemas/CreatedBy'
 *          deletedBy:
 *            type: object
 *            $ref: '#/components/schemas/CreatedBy'
 *          appName:
 *            type: object
 *            items:
 *              type: string
 *              format: byte
 *          stateName:
 *            type: object
 *            items:
 *              type: string
 *              format: byte
 *          createdAt:
 *            type: string
 *            format: date
 *          updatedAt:
 *            type: string
 *            format: date
 *          deletedAt:
 *            type: string
 *            format: date
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      CollegeForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              active:
 *                type: boolean
 *              collegeName:
 *                type: string
 *              city:
 *                type: string
 *              stateId:
 *                type: string
 *                format: uuid
 *
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      collegeForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates: 
 *            type: object
 *            properties:
 *              active:
 *                type: boolean
 */