module.exports = (sequelize, type) => {
    const schedule = sequelize.define('schedule', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        title: {
            type: type.TEXT,
            allowNull: false
        },
        description: {
            type: type.JSONB,
            allowNull: true,
        },
        startDate: {
            type: type.DATEONLY,
            allowNull: true
        },
        endDate: {
            type: type.DATEONLY,
            allowNull: true
        },
        active: {
            type: type.BOOLEAN,
            allowNull: true,
            defaultValue: true
        },
        type: {
            type: type.INTEGER,
            allowNull: false
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        }
    }, {
        underscored: true,
        freezeTableName: true
    })

    // schedule relationship
    schedule.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    schedule.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })

    schedule.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })
    schedule.belongsTo(sequelize.model('classes'),
        {
            as: 'class',
            foreignKey: {
                name: 'classId',
                allowNull: true
            }
        })
    return schedule
}

/**
 * @swagger
 *  components:
 *    schemas:
 *      ScheduleWithoutId:
 *        type: object
 *        required:
 *          - title
 *          - description
 *          - active
 *          - startDate
 *          - endDate
 *          - classId
 *          - type
 *        properties:
 *          title:
 *              type: string
 *          description:
 *              type: array
 *              items:
 *                  type: object
 *                  $ref: '#/components/schemas/MccqExplanation'
 *          active:
 *            type: boolean
 *          startDate:
 *            type: string
 *            format: date
 *          endDate:
 *            type: string
 *            format: date
 *          classId:
 *            type: string
 *            format: uuid
 *          type:
 *            type: string
 *            enums: [page,externalLink]
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      ScheduleForUpdate:
 *        type: object
 *        properties:
 *          title:
 *              type: string
 *          description:
 *              type: array
 *              items:
 *                  type: object
 *                  $ref: '#/components/schemas/MccqExplanation'
 *          active:
 *            type: boolean
 *          startDate:
 *            type: string
 *            format: date
 *          endDate:
 *            type: string
 *            format: date
 *          classId:
 *            type: string
 *            format: uuid
 *          type:
 *            type: string
 *            enums: [page,externalLink]
 */




/**
 * @swagger
 *  components:
 *    schemas:
 *      ScheduleWithId:
 *        type: object
 *        properties:
 *          uuidIdentifier:
 *            type: uuid
 *          title:
 *              type:
 *          description:
 *              type: array
 *              items:
 *                  type: object
 *                  $ref: '#/components/schemas/MccqExplanation'
 *          createdBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          updatedBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          deletedBy:
 *              type: object
 *              $ref: '#/components/schemas/CreatedBy'
 *          createdAt:
 *              type: string
 *              format: date-time
 *          updatedAt:
 *              type: string
 *              format: date-time
 *          deletedAt:
 *              type: string
 *              format: date-time
 *          active:
 *              type: boolean
 *          startDate:
 *            type: string
 *            format: date
 *          endDate:
 *            type: string
 *            format: date
 *          classId:
 *            type: string
 *            format: uuid
 *          type:
 *            type: string
 *            enums: [page,externalLink]
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      ScheduleForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates:
 *            type: object
 *            properties:
 *              recover:
 *                type: boolean
 *              active:
 *                type: boolean
 *          type:
 *            type: string
 *            enums: [page,externalLink]
 */