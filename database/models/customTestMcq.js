module.exports = (sequelize, type) => {
    const customTestMcq = sequelize.define(
        "custom_test_mcq",
        {
            id: {
                type: type.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            uuidIdentifier: {
                type: type.UUID,
                defaultValue: type.UUIDV4,
                allowNull: false
            },
            visitCount: {
                type: type.INTEGER,
                allowNull: true
            },
            customTestId:{
                type: type.ARRAY(type.UUID),
                allowNull: true
            }
        },
        {
            underscored: true,
            freezeTableName: true
        }
    );
    // customTest relationship
    customTestMcq.belongsTo(sequelize.model('user'), {
        foreignKey: {
            allowNull: false
        }
    })
    customTestMcq.belongsTo(sequelize.model('mcq'), {
        foreignKey: {
            allowNull: false
        }
    })
    customTestMcq.belongsTo(sequelize.model('subject'), {
        foreignKey: {
            allowNull: false
        }
    })
    customTestMcq.belongsTo(sequelize.model('topic'), {
        foreignKey: {
            allowNull: false
        }
    })
    // customTestMcq.belongsTo(sequelize.model('custom_test'), {
    //     foreignKey: {
    //         allowNull: false
    //     }
    // })


    return customTestMcq;
};