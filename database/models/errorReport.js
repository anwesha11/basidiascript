module.exports = (sequelize, type) => {
    const errorReport = sequelize.define('errorReport', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        contentType: {
            type: type.INTEGER,
            allowNull: false
        },
        errorMessage: {
            type: type.INTEGER,
            allowNull: false
        },
        errorText: {
            type: type.TEXT,
            allowNull: false
        },
        status: {
            type: type.INTEGER,
            allowNull: false
        },
        replied: {
            type: type.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        errorReportNumber: {
            type: type.INTEGER,
            allowNull: false
        },
        deletedAt: {
            type: type.DATE,
            allowNull: true
        },
        contentId:{
            type: type.UUID,
            allowNull: false
        }
    }, {
        underscored: true,
        freezeTableName: false
    });

    errorReport.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })

    errorReport.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: true
            }
        })

    errorReport.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })
    errorReport.belongsTo(sequelize.model('subject'),
        {
            foreignKey: {
                allowNull: false
            }
        })
        errorReport.belongsTo(sequelize.model('classes'),
        {
            as:'class',
            foreignKey: {
                name: 'classId',
                allowNull: true
            }
        })
    return errorReport;
};


/**
 * @swagger
 *  components:
 *    schemas:
 *      ErrorReportWithoutId:
 *        type: object
 *        required:
 *          - contentType
 *          - errorMessage
 *          - errorText
 *          - subjectId
 *          - contentId
 *          - classId
 *        properties:
 *          contentType:
 *            type: string
 *            enum: [mcq, keys, multipleCorrectChoiceQuestion, arrangeTheFollowing, videos]
 *          errorMessage:
 *            type: string
 *            enum: [fact, confusing, explanation]
 *          errorText:
 *            type: string
 *          subjectId:
 *            type: string
 *            format: uuid
 *          contentId:
 *            type: string
 *            format: uuid
 *          classId:
 *            type: string
 *            format: uuid
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      ErrorResponse:
 *        type: object
 *        required:
 *          - title
 *          - subTitle
 *          - module
 *          - moduleId
 *          - linkType
 *          - subject
 *          - toMail
 *          - data
 *        properties:
 *          title:
 *              type: string
 *          subTitle:
 *              type: string
 *          module:
 *              type: string
 *          moduleId:
 *              type: string
 *              format: uuid
 *          linkType:
 *              type: string
 *              enum: [ openApp, deepLink]
 *          imageUrl:
 *              type: string
 *              format: uri
 *          subject: 
 *              type: string
 *          toMail:
 *              type: string
 *              format: email
 *          data: 
 *              type: string
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      ErrorReportForUpdate:
 *        type: object
 *        properties:
 *          status:
 *            type: string
 *            enum: [resolved, active, escalated]
 *          replied:
 *            type: boolean
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      ErrorReportWithId:
 *        type: object
 *        properties:
 *          uuidIdentifier:
 *            type: uuid
 *          contentType:
 *            type: string
 *            enum: [mcq, keys, multipleCorrectChoiceQuestion, arrangeTheFollowing, videos]
 *          errorMessage:
 *            type: string
 *            enum: [fact, confusing, explanation]
 *          errorText:
 *            type: string
 *          subjectId:
 *            type: string
 *            format: uuid
 *          replied:
 *            type: boolean
 *          status:
 *            type: string
 *            enum: [resolved, active, escalated]
 *          createdBy:
 *            type: string
 *            format: uuid
 *          updatedBy:
 *            type: string
 *            format: uuid
 *          deletedBy:
 *            type: string
 *            format: uuid
 *          createdAt:
 *            type: string
 *            format: date
 *          updatedAt:
 *            type: string
 *            format: date
 *          deletedAt:
 *            type: string
 *            format: date
 *          classId:
 *            type: string
 *            format: uuid
 */


/**
 * @swagger
 *  components:
 *    schemas:
 *      ErrorReportForBulkUpdate:
 *        type: object
 *        required:
 *          - ids
 *          - updates
 *        properties:
 *          ids:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          updates: 
 *            type: object
 *            properties:
 *              status:
 *                type: string
 *                enum: [resolved, active, escalated]
 *              active:
 *                type: boolean 
 */