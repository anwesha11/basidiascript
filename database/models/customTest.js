module.exports = (sequelize, type) => {
    const customTest = sequelize.define(
        "custom_test",
        {
            id: {
                type: type.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },

            uuidIdentifier: {
                type: type.UUID,
                defaultValue: type.UUIDV4,
                allowNull: false
            },
            name: {
                type: type.STRING,
                allowNull: false
            },
            active: {
                type: type.BOOLEAN,
                allowNull: false,
                defaultValue: true
            },
            noOfQuestion: {
                type: type.INTEGER,
                allowNull: true
            },
            timeTaken: {
                type: type.INTEGER,
                allowNull: false
            },
            duration: {
                type: type.INTEGER,
                allowNull: false
            },
            deletedAt: {
                type: type.DATE,
                allowNull: true
            }
        },
        {
            underscored: true,
            freezeTableName: true
        }
    );
    // customTest relationship
    customTest.belongsTo(sequelize.model('user'),
        {
            as: 'creationUser',
            foreignKey: {
                name: 'createdBy',
                allowNull: false,
            }
        })
    customTest.belongsTo(sequelize.model('user'),
        {
            as: 'updationUser',
            foreignKey: {
                name: 'updatedBy',
                allowNull: false
            }
        })
    customTest.belongsTo(sequelize.model('user'),
        {
            as: 'deletionUser',
            foreignKey: {
                name: 'deletedBy',
                allowNull: true
            }
        })

    customTest.belongsTo(sequelize.model('subject'),
        {
            foreignKey: {
                type: type.UUID,
                allowNull: true
            }
        })

    customTest.belongsTo(sequelize.model('hashtag'),
        {
            foreignKey: {
                type: type.UUID,
                allowNull: true
            }
        })

    customTest.belongsTo(sequelize.model('topic'),
        {
            foreignKey: {
                type: type.UUID,
                allowNull: false
            }
        })
    sequelize.model('topic').hasMany(customTest)
    customTest.belongsTo(sequelize.model('user'), {
        foreignKey: {
            allowNull: false
        }
    })

    return customTest;
};


/**
 * @swagger
 *  components:
 *    schemas:
 *      customTestWithoutId:
 *        type: object
 *        required:
 *          - topicId
 *          - noOfQuestion
 *          - name
 *          - active
 *          - timeTaken
 *        properties:
 *          noOfQuestion:
 *              type: string
 *          name:
 *              type: string
 *          timeTaken:
 *              type: integer
 *          topicId:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          subjects:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 *          hashtags:
 *            type: array
 *            items:
 *              type: string
 *          active:
 *            type: boolean
 */


/**
* @swagger
*  components:
*    schemas:
*      CustomTestWithId:
*        type: array
*        properties:
*          uuidIdentifier:
*            type: uuid
*          question:
*              type: string
*          subjectId:
*              type: string
*              format: uuid
*          customTestId:
*              type: string
*              format: uuid
*          topicId:
*              type: string
*              format: uuid
*          subjectName:
*              type: string
*          topicName:
*              type: string
*          questionImage:
*              type: string
*          option1:
*              type: string
*          option2:
*              type: string
*          option3:
*              type: string
*          option4:
*              type: string
*          questionHtml:
*              type: string
*          option1Html:
*              type: string
*          sortNumber:
*              type: integer
*              format: int32
*          option2Html:
*              type: string
*          option3Html:
*              type: string
*          option4Html:
*              type: string
*          correctOption:
*              type: integer
*              format: int32
*          correctAnswerPercentage:
*              type: integer
*              format: int32
*/


/**
* @swagger
*  components:
*    schemas:
*      CustomTestWithViewDetails:
*        type: array
*        properties:
*          uuidIdentifier:
*            type: uuid
*          name:
*              type: string
*          topicId:
*            type: array
*            items:
*              type: string
*              format: uuid
*          duration:
*              type: integer
*              format: int32
*          timeTaken:
*              type: integer
*              format: int32
*          noOfQuestion:
*              type: integer
*              format: int32
*/


/**
 * @swagger
 *  components:
 *    schemas:
 *      customTestId:
 *        type: object
 *        required:
 *          - topicId
 *        properties:
 *          topicId:
 *            type: array
 *            items:
 *              type: string
 *              format: uuid
 */



/**
* @swagger
*  components:
*    schemas:
*      CustomTestForUpdate:
*        type: object
*        required:
*          - topicId
*          - noOfQuestion
*          - name
*          - active
*          - timeTaken
*        properties:
*          noOfQuestion:
*              type: string
*          name:
*              type: string
*          timeTaken:
*              type: integer
*          topicId:
*            type: array
*            items:
*              type: string
*              format: uuid
*          active:
*            type: boolean
*/