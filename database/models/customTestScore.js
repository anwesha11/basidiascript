module.exports = (sequelize, type) => {
    const customTestScore = sequelize.define(
        "custom_test_score",
        {
            id: {
                type: type.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            uuidIdentifier: {
                type: type.UUID,
                defaultValue: type.UUIDV4,
                allowNull: false
            },
            userTotalScore: {
                type: type.INTEGER,
                allowNull: true
            },
            totalCorrectCount: {
                type: type.INTEGER,
                allowNull: true
            },
            totalSkippedCount: {
                type: type.INTEGER,
                allowNull: true
            },
            totalWrongCount: {
                type: type.INTEGER,
                allowNull: true
            },
            totalScore: {
                type: type.INTEGER,
                allowNull: true
            },
            physicsUnattemptedAnswer: {
                type: type.INTEGER,
                allowNull: true
            },
            physicsActualScore: {
                type: type.INTEGER,
                allowNull: true
            },
            physicsCorrectAnswer: {
                type: type.INTEGER,
                allowNull: true
            },
            physicsInCorrectAnswer: {
                type: type.INTEGER,
                allowNull: true
            },
            physcicsScore: {
                type: type.INTEGER,
                allowNull: true
            },
            chemistryActualScore: {
                type: type.INTEGER,
                allowNull: true
            },
            chemistryUnattemptedAnswer: {
                type: type.INTEGER,
                allowNull: true
            },
            chemistryScore: {
                type: type.INTEGER,
                allowNull: true
            },
            chemistryCorrectAnswer: {
                type: type.INTEGER,
                allowNull: true
            },
            chemistryInCorrectAnswer: {
                type: type.INTEGER,
                allowNull: true
            },
            zoologyScore: {
                type: type.INTEGER,
                allowNull: true
            },
            zoologyInCorrectAnswer: {
                type: type.INTEGER,
                allowNull: true
            },
            zoologyUnattemptedAnswer: {
                type: type.INTEGER,
                allowNull: true
            },
            zoologyCorrectAnswer: {
                type: type.INTEGER,
                allowNull: true
            },
            zoologyActualScore: {
                type: type.INTEGER,
                allowNull: true
            },
            botanyActualScore: {
                type: type.INTEGER,
                allowNull: true
            },
            botanyScore: {
                type: type.INTEGER,
                allowNull: true
            },
            botanyCorrectAnswer: {
                type: type.INTEGER,
                allowNull: true
            },
            botanyInCorrectAnswer: {
                type: type.INTEGER,
                allowNull: true
            },
            botanyUnattemptedAnswer: {
                type: type.INTEGER,
                allowNull: true
            },
            deletedAt: {
                type: type.DATE,
                allowNull: true
            }
        },
        {
            underscored: true,
            freezeTableName: true
        }
    );
    // customTest relationship
    
    customTestScore.belongsTo(sequelize.model('user'), {
        foreignKey: {
            allowNull: false
        }
    })
    customTestScore.belongsTo(sequelize.model('custom_test'), {
        foreignKey: {
            allowNull: false
        }
    })


    return customTestScore;
};