module.exports = (sequelize, type) => {
    const payment = sequelize.define('payment', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        orderId: {
            type: type.STRING,
            allowNull: false
        },
        amount: {
            type: type.FLOAT,
            allowNull: false
        },
        paymentStatus: {
            type: type.STRING,
            allowNull: true
        },
        voucherUsed: {
            type: type.STRING,
            allowNull: true
        },
        subscriptionPlanDuration: {
            type: type.INTEGER,
            allowNull: false
        },
        paymentDate: {
            type: type.DATE,
            allowNull: true
        },
        components: {
            type: type.STRING,
            allowNull: false
        },
        invoiceNumber: {
            type: type.STRING,
            allowNull: true
        },
        location: {
            type: type.STRING,
            allowNull: true
        }
    }, {
        freezeTableName: true,
        underscored: true
    });
    payment.belongsTo(sequelize.model('user'), {
        foreignKey: {
            allowNull: false
        }
    })
    payment.belongsTo(sequelize.model('subscription'), {
        foreignKey: {
            allowNull: false
        }
    })
    return payment;
};