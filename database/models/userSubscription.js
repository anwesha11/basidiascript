module.exports = (sequelize, type) => {
    const userSubscription = sequelize.define('userSubscription', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        uuidIdentifier: {
            type: type.UUID,
            defaultValue: type.UUIDV4,
            allowNull: false
        },
        subscriptionStatus: {
            type: type.BOOLEAN
        },
        subscribedOn: {
            type: type.DATEONLY
        },
        subscriptionValidity: {
            type: type.DATEONLY
        }
    }, {
        underscored: true,
        freezeTableName: false
    });

    userSubscription.belongsTo(sequelize.model('user'), {
        foreignKey: {
            allowNull: false
        }
    })
    userSubscription.belongsTo(sequelize.model('subscription'), {
        foreignKey: {
            allowNull: false
        }
    })
    sequelize.model('user').hasMany(userSubscription)
    return userSubscription;
};